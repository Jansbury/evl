<?php
/*
include("Mobile_Detect.php");
$detect = new Mobile_Detect();
if ($detect->isMobile() && isset($_COOKIE['mobile']))
{
$detect = "false";
}
if( $detect->isMobile() && !$detect->isTablet() )
{
header("Location:http://www.nzcapitalgroup.co.nz/m");
}
*/
?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />	
        <title>NZCG Finance | Car Loan | Personal Loan | Debt Consolidation</title>
        <meta name="description" content="APPLY ONLINE for a CAR LOAN, PERSONAL LOAN or DEBT CONSOLIDATION through NZCG. Financing your Journey through life. (0800) 002 844">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="css/main.min.css">
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
        
    </head>
    <body>
    <div class="preloader">
	    <div class="preloaderText">
				<img src="i/loader.png" alt="loaderimage"/>	    	
	    </div>
	    
    </div>
    		<!--[if lt IE 9]> <style>
    		.gradient {filter: none;}</style>
    		<link href="css/ie.css" rel="stylesheet" type="text/css" />
    		<script src="js/ie.js"></script>
    		<![endif]-->
        <!--[if lt IE 9]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
		<header>
		<div class="headerBkg"></div>
		<div class="cntr clearfix">
			<div class="logo home-link">
				<img src="i/logo_sm.svg" alt="logo_thumb" class="svg"/> 
				<!--[if IE]><img src="i/logo_sm.png" alt="logo_thumb"/><![endif]-->
			<span class="logoText">Executive Vehicle Leasing</span>
			</div>
			<nav class="main" id="menu">
					<ul>
						<li data-menuanchor="welcome" class="active"><a href="#welcome">Welcome</a></li>
						<li data-menuanchor="carloan"><a href="#carloan">Car Loan</a></li>
						<li data-menuanchor="personalloan"><a href="#personalloan">Personal Loan</a></li>
						<li data-menuanchor="dealerfinance"><a href="#dealerfinance">Dealer Finance</a></li>
						<li data-menuanchor="contact"><a href="#contact">Contact</a></li>
					</ul>
				<div class="mainUnderline"></div>
				
			</nav>

		</div>
		</header>
		


<div class="section" id="section0">
	<div class="slide">
		<div class="homeImage"></div>

			<div class="cntrSlideHome clearfix">
		
				<div class="homeText">
					
					<h3>Apply online today.<br /> We can help you with<br />car loans, personal loans <br />and debt consolidation</h3>
					
					
					<p>We understand that everyone is on 
					a different journey through life and we 
					respect that. We work hard to customise the 
					right lending solution for you; no matter what 
					direction life is taking.</p>

					<p>As a specialist boutique lender you may take 
					confidence from our personalised approach 
					that we understand what truly matters.</p>
				</div>
								
			</div>
	</div>
	<div class="slide" id="about">
		<div class="cntrSlide">
			<h1>About Us.</h1>
			
			<div class="aboutCon clearfix">
				<div class="about1">
					
					<span class="titles">Small enough to care and large<br /> enough to deliver</span>
					
					<p>NZ Capital Group Limited (NZCG) set out from humble beginnings with a singular vision; to empower all New Zealanders, regardless of circumstances to reach their financial goals; and is fast becoming New Zealand’s emerging leader in innovative financial solutions. As a lender and not a broker NZCG is better positioned to turn your financial goals into reality.</p>

					<p>Life takes each one of us on very different journeys, each is precious and unique and at NZCG we work with you to truly understand where you’ve come from and where you want to go.</p>
					
					<span class="titles">Join the team</span>
					<p>NZ Capital Group is always looking for self-motivated individuals with a passion for the business we do and the high levels of customer service we pride ourselves on delivering.</p>
					<p>As an organisation, we believe that only through our people will the company succeed. If you think this is you then please <span class="blue contact-link">contact us</span> today.</p>

					<div class="blueButt contact-link">Contact Us</div>
				
					
				</div>				
				<div class="about2">
					<span class="titles">Responsible Lending</span>
					
					<p><span class="l-reg">What you should expect when borrowing money from NZCG. Understanding your needs</span>
						As a Responsible Lender we will do everything we can to understand you and your situation, so you can make good decisions about your loan.
					</p>
					<p><span class="l-reg">Deciding if the loan is right for you</span>
						We will then work out whether you are able to comfortably repay your loan and if the loan meets your needs or goals.
					</p>
					<p><span class="l-reg">Making sure you understand</span>
						We will do our very best to make sure you understand everything about your loan, including your rights and responsibilities, before you sign a contract.
					</p>
					<p><span class="l-reg">Helping you if things go wrong</span>
						We will treat you fairly and reasonably if you miss payments. This may include renegotiating the terms of the contract and working with you to find solutions if you suddenly face hardship.
					</p>
					
					
					<span class="titles">A hands-on personalised service</span>
					<p>The NZCG team is comprised of a group of sharp minds from the Lending and Financial Services Industry. The breadth of experience and ingenuity enables NZCG to present our customers with the best proposition possible.</p>
				
				</div>				
				
			</div>
			
		
		
			
			
		</div>
	</div>
</div>

<!-- AUTO LOAN -->
<div class="section" id="section1">
	<!-- AUTO LOAN - SLIDE 1-->
    <div class="slide" id="auto">
    	<div class="cntrSlide clearfix">
				<h1>Car Loan</h1>
				
				<div class="auto clearfix">
					<span class="titles">No matter where you are in life we can put you in the driving seat…</span>
					<p>As a boutique independent lender we understand that everyone’s circumstances are different.</p>
					<p>Maybe it’s your first car, a few credit glitches, new to work or casual employment; chances are we have encountered it all before and will be able to structure a car loan that works for you.</p>
					<p>Our specialist lending consultants will work directly with you to ensure that buying your new car, whatever it may be is a simple,
						hassle-free and pleasurable experience.</p>
					
					<span class="titles">Am I eligible?</span>
					<p>You need to be at least 18 years of age, a permanent New Zealand resident and receive a regular income to apply.</p>
	
					<span class="detailText">*All NZCG Loans are subject to approval and NZCG Credit Criteria.</span>
				
					<div class="blueButt auto-link">Apply Here</div>
					<span class="fsp"><a href="pdf/FDR_Disclosure_(NZCG).pdf" target="_blank">FSP Disclosure</a></span>
				</div>
				<div class="autoHeaderCon">
					<div class="autoHeader aone">
						Enjoy No Deposit regardless of your circumstances
					</div>
					<div class="autoHeader atwo">
						Interest rates from 12.95% p.a. fixed for the life of the loan
					</div>
					<div class="autoHeader athr">
						The freedom to buy what you want, where you want
					</div>
				</div>
    	</div>
    		
			
				
    	
		</div>
		<!-- AUTO LOAN - SLIDE 2-->
    <div class="slide">
			<div class="cntrSlide">
				<h1>Car Loan</h1>
				
					<div class="auto">
						<span class="titles">Drive away today with 1 hour approvals, <br />7 days a week.</span>
						
						<ol class="carProcess">
							<li>
								<div>
									Decide how much you want to borrow.
									<p>Our car loans are no deposit, but having a deposit or trade-in reduces rge amount you will need to borrow, reducing your repayments</p>
								</div>
							</li>
							<li>
								<div>
									Repayments to suit
									<p>	Decide how much you can comfortably afford to repay each pay day. To assist we offer repayment flexibility with weekly, fortnightly or monthly repayments by direct debit or wage deduction with up to five years to pay.
									<br />
									<br />
									To get an idea, see our Repayment Calculator.</p>
								</div>
							</li>
							<li>
								<div>Easy online application
									<p>Simply apply online and receive a credit decision within 1 hour of sending us your ID and Bank Statements.</p>
								</div>
							</li>
							<li>
								<div>
									Buy your car
									<p>	Your pre-approval will be valid for up to 60 days giving you time to shop around if you haven’t already found the car you want to buy. Please remember that the car being purchased will need to meet our security requirements. Simply call us with the registration number on 0800 002 844 and we can tell you instantly if it’s good value or otherwise.
									</p>
								</div>
							</li>
						</ol>
							<div class="wywn">	
							<span class="titles">What you will need</span>
							<p>To support your online application and so that we can provide you with a quick credit decision, please send us the following when applying for a Car Loan:</p>
					
								<div class="wywnList clearfix">
								   <ol>
								      <li><p>A clear copy of your current Restricted for Full Drivers Licence (cannot accept Learners)</p></li>
								      <li><p>Two months recent, concurrent Bank Statements showing all transactions</p></li>
								      <li><p>Details of the car you want to buy (if known)</p></li>
								   </ol>
								</div>
							
						</div><!-- end .wywn -->
					</div><!-- end .auto -->
									
					<div class="diagramCon">
						<img src="i/progress-01.png" alt="processAuto"/>
						
					</div>
				
    	</div>
		</div>
		<!-- AUTO LOAN - SLIDE 3 -->
    <div class="slide">
			<div class="cntrSlide">
				<h1>Car Loan</h1>
				<div class="auto">
					<span class="titles">Minimum Security Requirements</span>
					<p>The vehicle serves as security for the loan and therefore must be comprehensively insured throughout the term of the loan. Minimum security value of $3,000 is required.</p>
					<p> The vehicle must also have a current Warrant of Fitness and Registration at the time of purchase and the applicant must hold a valid full or restricted NZ Driver Licence.</p>
					<span class="titles">Credit Contract Indemnity Insurance</span>
					<p>Credit Contract Indemnity Insurance can protect you and your family's financial situation in event of the unexpected including Death, Accident, Illness, Redundancy, Business Interruption, Bankruptcy, Suspension and Hospitalisation that occur during the period of insurance (term of the loan agreement).
					</p>
					<p>For the full terms, conditions and benefit, please refer to the <a href="pdf/CCI_Policy_Booklet.pdf" target="_blank">policy booklet.</a></p>

					

					
						<div class="blueButt auto-link">
							Apply Here
						</div>	
				</div>
				
				<div class="animation">
		    	<div class="ani-col">
			    	<span class="infoOne-1">
			    		<span>Credit Checking & PPSR Registration
							<span class="p">$31.00</span>
						</span>
			    	</span>
			    	<span class="infoOne-2">
				    	<span>Application and Documentation
				    	<span class="p">$275 - $570</span></span>
			    	</span>
		    	</div>
		    	<div class="ani-colMid">
		    		<span class="infoOne-3">
			    		<span>Payment handling
			    		<span class="p">$1.50 (per week)</span></span>
		    		</span>
		    		<span class="infoOne-4">
			    		<span>To vary the loan
			    		<span class="p">$95.00</span></span>
		    		</span>
		    		<span class="infoOne-5">
			    		<span>To top-up the loan
			    		<span class="p">$10.00</span></span>
		    		</span>
		    	</div>
		    	<div class="ani-col">
		    		<span class="infoOne-6">
							<span>To install GPS Tracking
								<span class="p">$504.85</span>
								<span class="p">$25.30 per month</span>	
							</span>
		    		</span>
		    		<span class="infoOne-7">
			    		<span>To repay the loan early
			    		<span class="p">$40.00</span></span>
		    		</span>
		    	</div>
				</div><!-- end .animation -->

				
				
				<div class="animationTwo">
					
					<p>NZ Capital Group offers low fees with no hidden surprises. All fees are included in the loan repayments.</p>
					<p>Other fees and charges may apply and are clearly listed and disclosed on the loan agreement.
						To avoid late payment fees please call us on 0800 002 844 if you need any assistance to restructure your payments 
						or help with a missed payment.</p>
				</div>
				
				
				
    	</div>
		</div>
		<!-- AUTO LOAN - SLIDE 4 -->
		 <div class="slide">
			<div class="cntrSlide">
			<h1>Car Loan</h1>
				<div class="applyMainCon clearfix">
					<div class="applyCon">	
							<div id="iframeAuto" class="applyForm onlite"></div>
					</div>
					
					<div class="faqAuto">
						<span class="titles no">Frequently Asked Questions</span>
						<ul>
							<li>How old do I need to be to get a loan? 
								<span>You must be over the age of 18.</span>
							</li>
							<li>What information do I need to apply for a loan? 
								<span>We require a Restricted or Full NZ drivers Licence, 60 days of recent Bank Statements and confirmation that any security being used, such as a car, is comprehensively insured. From time to time we may require other documents such as a payslip or proof of address.</span>
							</li>
							<li>Do I need a deposit? 
								<span>In most cases a deposit is not required, however if you do not meet our normal lending criteria we may require some form of equity or the installation of GPS Tracking where a vehicle is being used as security.  </span>
							</li>
							<li>Do I need a good credit history? 
								<span>Preferably, but not necessarily. If you have debts recorded against your name these must be paid or in the process of being paid. If you have unpaid defaults then we may still be able to help you into a new car with the GPS Tracking add-on.</span>
							</li>
							<li>What is GPS Tracking?
								<span>GPS Tracking can help you secure a loan for a new car where you have bad credit or do not meet other aspects of our normal lending criteria. The interest is slightly higher and fees apply. In the event of missed payments we may locate the vehicle or prevent the vehicle from being started. GPS Tracking may also help reduce the cost of insurance as we can locate the vehicle in the event of it being stolen.  </span>
							</li>
							<li>Why would I need a guarantor or co-borrower? 
								<span>A Guarantor or Co-borrower may be a partner, parent, in-law, or guardian who together with your credit record may give us sufficient comfort with respect to your credit worthiness. The guarantor or co-borrower may be asked to support your application where your affordability is questionable, or you have a limited credit history. </span>
							</li>
							<li>What terms do you offer? 
								<span>Our minimum term is 6 months and our maximum is 60 months. We can also structure the loan to have a deferred start with differing repayment amounts. We could also set up a balloon (”residual”) payment at the conclusion of the loan.</span>
							</li>
							<li>What is your minimum and maximum loan? 
								<span>Minimum $1,000 Maximum $50,000. We can lend in excess of $50,000 however this is subject to sufficient security. </span>
							</li>
							<li>How long does it take to get a loan application approved?
							<span>We try to maintain an approval time of 1 hour from the time of your application. This may vary depending on the accuracy and complexity of the application. 
</span> </li>
							<li>Where do I sign the documents?
								<span>We will forward documents to you at home, or place of work for your convenience. This may be by email, registered post or alternatively you can collect the documents from our office. However we will need the original loan documents returned to us before we can pay out the loan.  </span>
							</li>
							<li>Are there any fees if I settle my loan early? 
								<span>Yes, there are fees for early repayment in full. The fees applicable to early settlement are set out clearly in the loan documentation. Interest is calculated daily right up to the date of prepayment, together with a small administration fee to cover costs. You may or may not be charged a Prepayment Loss calculated in accordance with the CCCFA 2003.   </span>
							</li>
							<li>What if I want to borrow more in the future? 
								<span>Not a problem as long as your repayment history with us is good. We will not generally lend further within a 6 month period, unless you are able to offer further security or indicate to us that you can afford a higher repayment level.
								</span></li>
							<li>What is debt consolidation? 
								<span>The combining of all your monthly bills into one. There are a number of benefits to debt consolidation. Once you have combined your debts you will have only one bill. You could also free up valuable household income as debt consolidation loans can often have a lower interest rate than the combined finance charges of all your other loans, hire purchases and credit cards. Debt consolidation loans usually require some form of security for the amount you have applied.</span>
							</li>
							<li>What is a secured loan? 
								<span>This is entirely up to you. It could be for debt consolidation, home renovations, retail purchases or even a holiday. </span>
							</li>
							<li>What is an unsecured loan? 
								<span>An unsecured loan does not use an asset for security. You must have clear credit however, demonstrate a proven credit history, and be in stable employment. If you have had previous history with our company we will view your application favourably so please advise us of this. Unsecured loans generally attract a higher interest rate than a secured loan. </span>
							</li>
							<li>What is Credit Contract Indemnity Insurance?
								<span>Credit Contract Indemnity Insurance is an optional add-on which covers your loan repayments if you can't work due to death, an accident, sickness, hospitalisation, redundancy, suspension or bankruptcy. Insurance is underwritten by VERO Insurance rated A+ by Standard and Poors. Terms & Conditions Apply.</span>
							</li>
							<li>What if you have been declined credit? 
								<span>If your application has been declined, then you can apply to Veda Advantage, or Dunn & Bradstreet to get a copy of your credit file. Give Veda a call on <a href="tel:0800692733">0800 692 733</a> or visit www.mycreditfile.co.nz </span>
							</li>
							<li>Why has my application been declined when my credit check shows no adverse?
								<span>You may have undisclosed loans registered on the Personal Properties Securities Register indicating that you do not have affordability for the loan. We recommend that you disclose ALL outstanding loans with other credit providers and where loans have been paid in full, request in writing from your credit provider, confirmation that any financing statement has been discharged.</span>
							</li>
						</ul>
						<span class="titles no">Downloads</span>
						<ul class="downloads">
							<li><a href="pdf/NZCG_Print_Application.pdf" target="_blank">Printable PDF application form</a></li>
							<li><a href="pdf/Fee_Schedule.pdf" target="_blank">Schedule of Fees and Charges</a></li>
							<li><a href="pdf/Anti-Money_Laundering.pdf" target="_blank">The new Anti Money Laundering Laws and how it may affect your loan application</a></li>
							<li><a href="pdf/Responsible_Lending_Guidlines.pdf" target="_blank">Responsible Lending Guidelines</a></li>
						</ul>
						
					</div>
					
				</div>
			
			</div>
		 </div>
		
</div>
<!-- PERSONAL LOAN -->
<div class="section" id="section2">
	<!-- PERSONAL LOAN - Slide 1 -->
    <div class="slide" id="personal">
			<div class="cntrSlide clearfix">
				<h1>Personal Loan</h1>
				<div class="personal">
					<span class="titles">Whatever you’re aiming for, an NZCG<br /> Personal Loan could help make it reality.<br /> From $3,000 to $50,000 for almost anything.</span>
					<p>Consolidate your debts into one low payment, renovate your home, help your children with a first home deposit or follow your dreams and start a new business. How you use your loan is up to you.</p>
					
					<span class="titles">Am I eligible?</span>
					<p>You need to be at least 18 years of age, a 
						permanent New Zealand resident and receive a regular income to apply.</p>

						<span class="detailText">*All NZCG Loans are subject to approval and NZCG Credit Criteria.</span>
						
						<div class="blueButt personal-link">Apply Now</div>
						<span class="fsp"><a href="pdf/FDR_Disclosure_(NZCG).pdf" target="_blank">FSP Disclosure</a></span>
						
					
						
				</div>
						<div class="flex">
							<span>Flexibility to lend more than the<br /> value of the security</span>
							
							<span>Interest rates from <span class="more">15.95% p.a.</span><br />Fixed for the term of the loan</span>
							
							<span>Up to <span class="more">5 years</span> to pay.<br />
							<span class="more">1 hour approval</span>, 7 days a week.</span>
						</div>	
						
							
				
				
				
    	</div>
		</div>
			<!-- PERSONAL LOAN - Slide 2 -->
    <div class="slide">
			<div class="cntrSlide white">
				<h1 class="onBlue">Personal Loan</h1>
				<div class="personal">
					<span class="titles">A Personal Loan from $3000 with 
						no maximum and up to 5 years to pay.</span>
						<p>Relax with an NZCG Personal Loan. It might be for home improvements, a way to consolidate your debts, the holiday you’ve worked so hard for, or the wedding you’ve always dreamed of.</p>
						<p>With flexible payment options and competitive interest rates, our Personal Loan is a great way to take control of your finances and make the most of today.</p>
						<div class="whiteButt personal-link">Apply Here</div>
							<div class="wywn">	
							<span class="titles">What you will need</span>
							<p>To support your online application and so that we can provide you with a quick credit decision, please send us the following when applying for a Personal Loan:</p>
								<div class="wywnList">
								   <ol>
								      <li><p>A clear copy of your current Restricted for Full Drivers Licence (cannot accept Learners)</p></li>
								      <li><p>Two months recent, concurrent Bank Statements showing all transactions</p></li>
								   </ol>
								</div>
							
						</div><!-- end .wywn -->
				</div>
									
					<div class="diagramCon">
						<img src="i/progress-02.png" alt="processAuto"/>
						
					</div>
    	</div>
		</div>
			<!-- PERSONAL LOAN - Slide 3 -->
    <div class="slide">
			<div class="cntrSlide white">
				<h1 class="onBlue">Personal Loan</h1>
				<div class="personal">
					<span class="titles">Minimum Security Requirements</span>
					<p>Where security is required for a loan, we will usually use a vehicle and therefore it must be comprehensively insured throughout the term of the loan. Minimum security value of $3,000 is required.</p>
					<p> The vehicle must also have a current Warrant of Fitness and Registration at the time of purchase and the applicant must hold a valid full or restricted NZ Driver Licence.</p>
					<span class="titles">Credit Contract Indemnity Insurance</span>
					<p>Credit Contract Indemnity Insurance can protect you and your family's financial situation in event of the unexpected including Death, Accident, Illness, Redundancy, Business Interruption, Bankruptcy, Suspension and Hospitalisation that occur during the period of insurance (term of the loan agreement).
					</p>
					<p>For the full terms, conditions and benefit, please refer to the <a href="pdf/CCI_Policy_Booklet.pdf" target="_blank">policy booklet.</a></p>
					
						<div class="whiteButt personal-link">Apply Here</div>
							

				</div>
				<div class="chartPer">
		    	<div class="chartPer-col">
			    	<span class="infoTwo-1">
			    		<span>Credit Checking & PPSR Registration
							<span class="p">$31.00</span></span>
			    	</span>
			    	<span class="infoTwo-2">
				    	<span>Application and Documentation
				    	<span class="p">$275 - $570</span></span>
			    	</span>
		    	</div>
		    	<div class="chartPer-colMid">
		    		<span class="infoTwo-3">
			    		<span>Payment handling
			    		<span class="p">$1.50 (per week)</span></span>
		    		</span>
		    		<span class="infoTwo-4">
			    		<span>To vary the loan
			    		<span class="p">$95.00</span></span>
		    		</span>
		    		<span class="infoTwo-5">
			    		<span>To top-up the loan
			    		<span class="p">$10.00</span></span>
		    		</span>
		    	</div>
		    	<div class="chartPer-col">
		    		<span class="infoTwo-6">
							<span>To register a Caveat
			    			<span class="p">$390.00</span>
			    		</span>
		    		</span>
		    		<span class="infoTwo-7">
			    		<span>To repay the loan early
			    		<span class="p">$40.00</span></span>
		    		</span>
		    	</div>
				</div><!-- end .chartPer -->
				
				<div class="chartPerTwo">
					<p>NZ Capital Group offers low fees with no hidden surprises. All fees are included in the loan repayments.</p>
					<p>Other fees and charges may apply and are clearly listed and disclosed on the loan agreement.
						To avoid late payment fees please call us on 0800 002 844 if you need any assistance to restructure your payments or help with a missed payment.</p>
				</div>

				
				
    	</div>
		</div>
	<!-- PERSONAL LOAN - Slide 4 -->
    <div class="slide">
			<div class="cntrSlide white">
				<h1 class="onBlue">Personal Loan</h1>
				<div class="applyMainCon clearfix">
					<div class="applyCon">
					<!--
	<div class="applyText">
						<span class="titles no">Loan Application</span>
							<p>Completing this form should take no more than 5 minutes. Once submitted we will call you to discuss yhou application.</p>
						</div>
-->
					
						<div class="applyForm"></div>
					</div>
					
					<div class="faq">
						<span class="titles no">Frequently Asked Questions</span>
						<ul>
							<li>How old do I need to be to get a loan? 
								<span>You must be over the age of 18.</span>
							</li>
							<li>What information do I need to apply for a loan? 
								<span>We require a Restricted or Full NZ drivers Licence, 60 days of recent Bank Statements and confirmation that any security being used, such as a car, is comprehensively insured. From time to time we may require other documents such as a payslip or proof of address.</span>
							</li>
							<li>Do I need a deposit? 
								<span>In most cases a deposit is not required, however if you do not meet our normal lending criteria we may require some form of equity or the installation of GPS Tracking where a vehicle is being used as security.  </span>
							</li>
							<li>Do I need a good credit history? 
								<span>Preferably, but not necessarily. If you have debts recorded against your name these must be paid or in the process of being paid. If you have unpaid defaults then we may still be able to help you into a new car with the GPS Tracking add-on.</span>
							</li>
							<li>What is GPS Tracking?
								<span>GPS Tracking can help you secure a loan for a new car where you have bad credit or do not meet other aspects of our normal lending criteria. The interest is slightly higher and fees apply. In the event of missed payments we may locate the vehicle or prevent the vehicle from being started. GPS Tracking may also help reduce the cost of insurance as we can locate the vehicle in the event of it being stolen.  </span>
							</li>
							<li>Why would I need a guarantor or co-borrower? 
								<span>A Guarantor or Co-borrower may be a partner, parent, in-law, or guardian who together with your credit record may give us sufficient comfort with respect to your credit worthiness. The guarantor or co-borrower may be asked to support your application where your affordability is questionable, or you have a limited credit history. </span>
							</li>
							<li>What terms do you offer? 
								<span>Our minimum term is 6 months and our maximum is 60 months. We can also structure the loan to have a deferred start with differing repayment amounts. We could also set up a balloon (”residual”) payment at the conclusion of the loan.</span>
							</li>
							<li>What is your minimum and maximum loan? 
								<span>Minimum $1,000 Maximum $50,000. We can lend in excess of $50,000 however this is subject to sufficient security. </span>
							</li>
							<li>How long does it take to get a loan application approved?
							<span>We try to maintain an approval time of 1 hour from the time of your application. This may vary depending on the accuracy and complexity of the application. 
</span> </li>
							<li>Where do I sign the documents?
								<span>We will forward documents to you at home, or place of work for your convenience. This may be by email, registered post or alternatively you can collect the documents from our office. However we will need the original loan documents returned to us before we can pay out the loan.  </span>
							</li>
							<li>Are there any fees if I settle my loan early? 
								<span>Yes, there are fees for early repayment in full. The fees applicable to early settlement are set out clearly in the loan documentation. Interest is calculated daily right up to the date of prepayment, together with a small administration fee to cover costs. You may or may not be charged a Prepayment Loss calculated in accordance with the CCCFA 2003.   </span>
							</li>
							<li>What if I want to borrow more in the future? 
								<span>Not a problem as long as your repayment history with us is good. We will not generally lend further within a 6 month period, unless you are able to offer further security or indicate to us that you can afford a higher repayment level.
								</span></li>
							<li>What is debt consolidation? 
								<span>The combining of all your monthly bills into one. There are a number of benefits to debt consolidation. Once you have combined your debts you will have only one bill. You could also free up valuable household income as debt consolidation loans can often have a lower interest rate than the combined finance charges of all your other loans, hire purchases and credit cards. Debt consolidation loans usually require some form of security for the amount you have applied.</span>
							</li>
							<li>What is a secured loan? 
								<span>This is entirely up to you. It could be for debt consolidation, home renovations, retail purchases or even a holiday. </span>
							</li>
							<li>What is an unsecured loan? 
								<span>An unsecured loan does not use an asset for security. You must have clear credit however, demonstrate a proven credit history, and be in stable employment. If you have had previous history with our company we will view your application favourably so please advise us of this. Unsecured loans generally attract a higher interest rate than a secured loan. </span>
							</li>
							<li>What is Credit Contract Indemnity Insurance?
								<span>Credit Contract Indemnity Insurance is an optional add-on which covers your loan repayments if you can't work due to death, an accident, sickness, hospitalisation, redundancy, suspension or bankruptcy. Insurance is underwritten by VERO Insurance rated A+ by Standard and Poors. Terms & Conditions Apply.</span>
							</li>
							<li>What if you have been declined credit? 
								<span>If your application has been declined, then you can apply to Veda Advantage, or Dunn & Bradstreet to get a copy of your credit file. Give Veda a call on <a href="tel:0800692733">0800 692 733</a> or visit www.mycreditfile.co.nz </span>
							</li>
							<li>Why has my application been declined when my credit check shows no adverse?
								<span>You may have undisclosed loans registered on the Personal Properties Securities Register indicating that you do not have affordability for the loan. We recommend that you disclose ALL outstanding loans with other credit providers and where loans have been paid in full, request in writing from your credit provider, confirmation that any financing statement has been discharged.</span>
							</li>
						</ul>
						<span class="titles no">Downloads</span>
						<ul class="downloads">
							<li><a href="pdf/NZCG_Print_Application.pdf" target="_blank">Printable PDF application form</a></li>
							<li><a href="pdf/Fee_Schedule.pdf" target="_blank">Schedule of Fees and Charges</a></li>
							<li><a href="pdf/Anti-Money_Laundering.pdf" target="_blank">The new Anti Money Laundering Laws and how it may affect your loan application</a></li>
							<li><a href="pdf/Responsible_Lending_Guidlines.pdf" target="_blank">Responsible Lending Guidelines</a></li>
						</ul>
						
					</div>
					
				</div>
    	</div>
		</div>
</div>
<!-- DEALER FINANCE -->
<div class="section" id="section3">
    <div class="slide" id="dealer">
			<div class="cntrSlide">
				<h1>Dealer Finance</h1>
				<div class="dealer">
					<span class="titles">You can now stop dealing with brokers<br /> and form a strong personal relationship<br /> with a lender you can trust to deliver.</span>
					<p>By being selective and limiting the numbers of motor vehicle dealers we forge relationships with, we can guarantee a higher level of service and as we start to understand your business, offer greater flexibility.</p>
					<p>We are not a last resort. We are a second tier lender, our loans are no deposit and our interest rates are fair and reflective of where we sit in the market place. We are not a loan factory. We are a boutique lender working hard to maximise your sales opportunities.</p>
					<p>You will find that we can quickly approve the majority of loans that fall outside of UDC, Marac, MTF and GE Money criteria.</p>
					
					
					
						<div class="blueButt register-link">
							Dealer Registration 
						</div>	
				</div>
				
				<div class="dealerTwo">
					<h3>Licensed motor vehicle dealer?</h3>
					<h3>Let us prove why we should<br /> be your ‘second-tier’ finance partner.</h3>
					
					<span class="dealerHeading">The NZCG Advantage:</span>
					<ul class="dealerli">
						<li>We listen and work hard to <span>say yes!</span></li>
						<li>Genuine No Deposit lending.</li>
						<li>Guaranteed fixed interest rate of <span>19.95% p.a.*</span></li>
						<li>1 hour decision.</li>
						<li><span>7 days a week</span> processing including public holidays.</li>
						<li>Competitive commissions and <span>freedom to sell your own warranties</span></li>
						<li>Customer referrals and on-yard marketing support.</li>
						<li>GPS and immobiliser options for non-conforming loan applications. </li>
					</ul>
					<div class="dealerdeat">*23.95% if non-conforming (GPS Tracking)</div>
				</div>
				
    	</div>
		</div>
		
		<div class="slide">
			<div class="cntrSlide">
				<h1>Dealer Finance</h1>
				<div class="dealer">
					<span class="titles">Hassle-free application process.</span>
					<p>Simply fax or email us a completed credit application form along with the Customer Information Notice (CIN) and a Restricted or Full Driver’s License for the primary borrower / driver.</p>
					<p>We will come back to you within the hour with a credit decision.</p>
					<p>You will find that we can quickly approve the majority of loans that fall outside of UDC, Marac, MTF and GE Money criteria.</p>
					<div class="dealerNote">Note: if we are unable to approve the loan as submitted we will come back with a maximum no deposit lend. Also, if the loan is obviously outside criteria we will come back with a decline almost instantly.</div>
					
					
						<div class="blueButt register-link">
							Dealer Registration
						</div>	
				</div>				
					<div class="diagramCon">
						<img src="i/progress-03.png" alt="processAuto"/>
						
					</div>
    	</div>
		</div>
		
    <div class="slide">
			<div class="cntrSlide">
				<h1>Dealer Finance</h1>
				<div class="applyMainCon clearfix">
					<div class="dealerApplyCon">
						<div class="applyText">
						<span class="titles no">Dealer Application</span>
							<p>If you wish to become an NZCG Partner please register your Dealership by completing the form below and we will be in touch.</p>
						</div>
					
						<div class="dealerFormCon clearfix">
                        
							    <form id="dealerForm" action="/nzcg/backoffice/DealerAppPage_Controller/Form" method="post">
								    
								     <div><input placeholder="Name of Dealership" type="text" class="formtext" id="d-dealership" name="d-dealership"></div>
								    
								    <div class="deal-left"> 
									     <div>
									        <input placeholder="Unit" type="text" class="formtext left" id="d-unit" name="d-unit">
									        <input placeholder="Number/Name" type="text" class="formtext right" id="d-numname" name="d-numname">
									     </div>
									     <div>
									        <input placeholder="Street" type="text" class="formtext" id="d-street" name="d-street">
									        <input placeholder="Suburb" type="text" class="formtext" id="d-suburb" name="d-suburb">
									     </div>
									     <div>
									        <input placeholder="City" type="text" class="formtext left" id="d-city" name="d-city">
									        <input placeholder="PostCode" type="text" class="formtext right" id="d-post" name="d-post">
									     </div>
								     </div>
							
							     <div class="deal-right">
								      <div>
								        <input placeholder="First Name" type="text" class="formtext" id="d-Fname" name="d-Fname">
								        <input placeholder="Last Name" type="text" class="formtext" id="d-Lname" name="d-Lname">
								      </div>
								         <div>
								        <input placeholder="Phone Number" type="text" class="formtext" id="d-phone" name="d-number">
								      </div>
								      <div>
								        <input placeholder="Email" type="email" class="formtext" id="d-email" name="d-email">
								      </div>
											
							     </div><!-- end .deal-right -->
							     		
							     		<div style="clear:both;">
												<p class="d-question">What finance company do you currently use for second tier loans?</p>
												<input placeholder="Company" type="text" class="formtext" id="d-second" name="d-second">
											</div>
							      <div>
							        <input class="radiobutton" type="submit" value="Submit Registration">
							      </div>
							    </form>

							
							
						</div><!-- end .dealerFormCon-->
					</div>
					
					<div class="dealerfaq">
						<span class="titles no">Frequently Asked Questions</span>
						<ul>
							<li>How much can you lend?
									<span>
										We can lend from $3,000 up to $50,000. All applicants must be able to afford the loan repayments without any undue financial stress.
									</span>
								</li>
								<li>What is a “Maximum Lend”?
									<span>
										This is the Maximum amount of finance we are willing to lend to one of your customers based on their credit profile and ability to service the repayments.
									</span>
								</li>
									<li>What terms do you offer?
										<span>Our minimum term is 6 months and our maximum is 60 months. </span>
									</li>
									<li>What is the minimum age of applicant you will lend to?
									<span>
											Applicants must be over the age of 18; however we often require a Guarantor where the applicant is under 20 years of age.</span></li>
									<li>Do you lend to Beneficiaries? 
									<span>We can lend to almost anyone including Beneficiaries as long as they can afford the repayments.</span></li>
									<li>Do my customers need a good credit history? 
									<span>
										Preferably, but not necessarily. If they have debts recorded against their name these must be paid or in the process of being paid. If they have unpaid defaults then we may still be able to help them into a new car with the GPS Tracking add-on.
										</span>
									</li>
									<li>What are the minimum application requirements?
										<span>
											We require a Restricted or Full NZ drivers Licence, 60 days of recent Bank Statements. Any further documentation will be requested if necessary after our lending team has reviewed the application.
										</span>
									</li>
									<li>Do my customers need a deposit or trade? 
										<span>In most cases a deposit is not required, however if they do not meet our normal lending criteria or the vehicle is overpriced we may require some form of equity or the installation of GPS Tracking.
										</span>
									</li>
									<li>What is GPS Tracking?
										<span>GPS Tracking is a GPS device and is installed where your customers have bad credit or do not meet other aspects of our normal lending criteria. The interest is slightly higher and fees apply. In the event of missed payments we may locate the vehicle or prevent the vehicle from being started. GPS Tracking may also help reduce the cost of insurance for your customers as the vehicle can be located in the event of it being stolen. 
										</span> 
									</li>
									</ul>
						
						<span class="titles no">How long does it take to get a loan application approved? </span>
						<p>We try to maintain an approval time of 1 hour from the time you send through an application. This may vary depending on the accuracy and complexity of the application. </p>
						
						<span class="titles no">How quickly is the loan paid out?</span>
						<p>As long as we have received the signed Loan Agreement and any supporting documents we require before 3:00PM on business days then the loan will be paid out that evening. </p>
						
						<span class="titles no">Can we add a Warranty? </span>
						<p>Absolutely! If the customer can afford the repayments then you may add your own Warranties.</p>
					
						
					</div>
					
				</div>

				
    	</div>
		</div>
</div>

<div class="section" id="section4">

    
    <div class="mapCon">
    
   
    <div id='map_canvas'></div>

					
				</div>

			<div class="conCon clearfix">
				<div class="con1">
					<div class="conHeading">Getting In Touch</div>
					
					<div id="contactForm">
	      		<div id="form">
					    <form class="contact-form form" id="customForm" action="mail-send.php" method="post">
					     	<div id="status" style="display: none;"></div>
					      <div class="leftContact">
					        <input placeholder="Name" type="text" class="formtext" id="name" name="name">
					        <input placeholder="Email" type="text" class="formtext" id="email" name="email">
					        <input placeholder="Subject" type="text" class="formtext" id="subject" name="subject">
					      </div>
					      <div class="rightContact">
					        <textarea placeholder="Message" name="message" id="message"></textarea>
					      </div>

					      <div>
					        <input class="radiobutton" type="submit" value="Send email">
					      </div>
					      <input id="spam" class="hidden" name="spam" type="text" value="">
					    </form>
					  </div>			
					</div>
					
				</div>

				<div class="con2">
					<ul>
						<li class="add">Level 3, Walker Wayland Centre, <br />
						53 Fort Street, Auckland, 1010</li>
						<li class="pos">PO Box 68591,<br /> Newton Auckland 1145</li>
						<li class="pho">0800 002 844</li>
						<li class="fax">0800 002 845</li>
						<li class="ema"><a href="mailto:info@nzcapitalgroup.co.nz">info@nzcapitalgroup.co.nz</a></li>
					</ul>
				</div>
				
								
    	</div>
    	<footer class="clearfix">
	    	<div class="footerCon clearfix">
		    	<div class="copy">&copy; <span id="copyYear">&nbsp;</span> NZ Capital Group Limited</div>
		    	
		    	<nav class="socialIcons">
			    	<ul>
			    		<li class="none"><a href="https://plus.google.com/108321124701962991935" target="_blank" rel="publisher"><div class="google">Google+</div></a></li>
				    	<li class="none"><a href="https://twitter.com/nzcapitalgroup" target="_blank"><div class="twitter">Twitter</div></a></li>
				    	<li class="none"><a href="https://www.facebook.com/nzcapitalgroup" target="_blank"><div class="facebook">Facebook</div></a></li>
			    	</ul>
		    	</nav>
		    	
		    	<nav class="footerNav">
			    	<ul>
				    	<li><a href="pdf/Terms_and_Conditions_(NZCG).pdf" target="_blank">Terms &amp; Conditions</a></li>
				    	<li><a href="pdf/Privacy_Disclosure_(NZCG).pdf" target="_blank">Privacy</a></li>
				    	<li><a href="pdf/FDR_Disclosure_(NZCG).pdf" target="_blank">FSP Disclosure</a></li>
				    	<li><a class="login" target="_blank" href="http://www.nzcapitalgroup.co.nz/backoffice"></a></li>
			    	</ul>
		    	</nav>
	    	</div>
    	</footer>
    	

</div>


		   	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
        <script src="js/jquery.slimscroll.min.js"></script>
        <script src="js/jquery.fullPage.min.js"></script>
        <script src="js/vendor/jquery-ui.js"></script>
        <script src="js/plugins.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC9ChB5JtmZwpiheyoWAN6ILpgUy2gMCoM&sensor=false"></script>
				<script src="js/main.js"></script>



  
			<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	
	  ga('create', 'UA-41471063-2', 'nzcapitalgroup.co.nz');
	  ga('send', 'pageview');

	</script>
    </body>
</html>
