(function($) {
	$.fn.fullpage = function(options) {
		options = $.extend({
			"verticalCentered": true,
			'resize': true,
			'slidesColor': [],
			'anchors': [],
			'scrollingSpeed': 700,
			'easing': 'easeInQuart',
			'menu': true,
			'navigation': false,
			'navigationPosition': 'right',
			'navigationColor': '#000',
			'navigationTooltips': [],
			'slidesNavigation': false,
			'slidesNavPosition': 'bottom',
			'controlArrowColor': '#fff',
			'loopBottom': false,
			'loopTop': false,
			'loopHorizontal': false,
			'autoScrolling': false,
			'scrollOverflow': false,
			'css3': false,
			'paddingTop': 0,
			'paddingBottom': 0,
			'fixedElements': null,
			'normalScrollElements': null,
			'afterLoad': null,
			'onLeave': null,
			'afterRender': null,
			'afterSlideLoad': null,
			'onSlideLeave': null
		}, options);
		var scrollDelay = 700;
		$.fn.fullpage.setAutoScrolling = function(value) {
			options.autoScrolling = value;
			var element = $('.section.active');
			if (options.autoScrolling) {
				$('html, body').css({
					'overflow': 'hidden',
					'height': '100%'
				});
				if (element.length) {
					if (options.css3) {
						var translate3d = 'translate3d(0px, -' + element.position().top + 'px, 0px)';
						transformContainer(translate3d, false)
					} else {
						$('#superContainer').css('top', '-' + element.position().top + 'px');
					}
				}
			} else {
				$('html, body').css({
					'overflow': 'auto',
					'height': 'auto'
				});
				if (options.css3) {
					var translate3d = 'translate3d(0px, 0px, 0px)';
					transformContainer(translate3d, false)
				} else {
					$('#superContainer').css('top', '0px');
				}
				$('html, body').scrollTop(element.position().top);
			}
		};
		var slideMoving = false;
		var isTablet = navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry|Windows Phone)/);
		var windowsHeight = $(window).height();
		var isMoving = false;
		var lastScrolledDestiny;
		addScrollEvent();
		if (options.css3) {
			options.css3 = support3d();
		}
		$('body').wrapInner('<div id="superContainer" />');
		if (options.navigation) {
			$('body').append('<div id="fullPage-nav"><ul></ul></div>');
			var nav = $('#fullPage-nav');
			nav.css('color', options.navigationColor);
			nav.addClass(options.navigationPosition);
		}
		$('.section').each(function(index) {
			var slides = $(this).find('.slide');
			var numSlides = slides.length;
			if (!index) {
				$(this).addClass('active');
			}
			$(this).css('height', windowsHeight + 'px');
			if (options.paddingTop || options.paddingBottom) {
				$(this).css('padding', options.paddingTop + ' 0 ' + options.paddingBottom + ' 0');
			}
			if (typeof options.slidesColor[index] !== 'undefined') {
				$(this).css('background-color', options.slidesColor[index]);
			}
			if (typeof options.anchors[index] !== 'undefined') {
				$(this).attr('data-anchor', options.anchors[index]);
			}
			if (options.navigation) {
				var link = '';
				if (options.anchors.length) {
					link = options.anchors[index];
				}
				var tooltip = options.navigationTooltips[index];
				if (typeof tooltip === 'undefined') {
					tooltip = '';
				}
				nav.find('ul').append('<li data-tooltip="' + tooltip + '"><a href="#' + link + '"><span></span></a></li>');
			}
			if (numSlides > 0) {
				var sliderWidth = numSlides * 100;
				var slideWidth = 100 / numSlides;
				slides.wrapAll('<div class="slidesContainer" />');
				slides.parent().wrap('<div class="slides" />');
				$(this).find('.slidesContainer').css('width', sliderWidth + '%');
				$(this).find('.slides').after('<div class="controlArrow prev"></div><div class="controlArrow next"></div>');
				$(this).find('.controlArrow.next').css('border-color', 'transparent transparent transparent ' + options.controlArrowColor);
				$(this).find('.controlArrow.prev').css('border-color', 'transparent ' + options.controlArrowColor + ' transparent transparent');
				if (!options.loopHorizontal) {
					$(this).find('.controlArrow.prev').hide();
				}
				if (options.slidesNavigation) {
					addSlidesNavigation($(this), numSlides);
				}
				slides.each(function(index) {
					if (!index) {
						$(this).addClass('active');
					}
					$(this).css('width', slideWidth + '%');
					if (options.verticalCentered) {
						addTableClass($(this));
					}
				});
			} else {
				if (options.verticalCentered) {
					addTableClass($(this));
				}
			}
		}).promise().done(function() {
			$.fn.fullpage.setAutoScrolling(options.autoScrolling);
			$.isFunction(options.afterRender) && options.afterRender.call(this);
			if (options.fixedElements && options.css3) {
				$(options.fixedElements).appendTo('body');
			}
			if (options.navigation) {
				nav.css('margin-top', '-' + (nav.height() / 2) + 'px');
				nav.find('li').first().find('a').addClass('active');
			}
		/*
	if (options.menu && options.css3) {
				$(options.menu).appendTo('body');
			}
*/
			if (options.scrollOverflow) {
				$(window).on('load', function() {
					$('.section').each(function() {
						var slides = $(this).find('.slide');
						if (slides.length) {
							slides.each(function() {
								createSlimScrolling($(this));
							});
						} else {
							createSlimScrolling($(this));
						}
					});
				});
			}
			$(window).on('load', function() {
				scrollToAnchor();
			});
		});
		var scrollId;
		var isScrolling = false;
		$(window).scroll(function(e) {
			if (!options.autoScrolling) {
				var currentScroll = $(window).scrollTop();
				var scrolledSections = $('.section').map(function() {
					if ($(this).offset().top < (currentScroll + 100)) {
						return $(this);
					}
				});
				var currentSection = scrolledSections[scrolledSections.length - 1];
				if (!currentSection.hasClass('active')) {
					isScrolling = true;
					var yMovement = getYmovement(currentSection);
					$('.section.active').removeClass('active');
					currentSection.addClass('active');
					var anchorLink = currentSection.data('anchor');
					$.isFunction(options.onLeave) && options.onLeave.call(this, currentSection.index('.section'), yMovement);
					$.isFunction(options.afterLoad) && options.afterLoad.call(this, anchorLink, (currentSection.index('.section') + 1));
					activateMenuElement(anchorLink);
					activateNavDots(anchorLink, 0);
					if (options.anchors.length && !isMoving) {
						lastScrolledDestiny = anchorLink;
						location.hash = anchorLink;
					}
					clearTimeout(scrollId);
					scrollId = setTimeout(function() {
						isScrolling = false;
					}, 100);
				}
			}
		});
		var touchStartY = 0;
		var touchStartX = 0;
		var touchEndY = 0;
		var touchEndX = 0;
		$(document).on('touchmove', function(event) {
			if (options.autoScrolling && isTablet) {
				event.preventDefault();
				var e = event.originalEvent;
				var touchMoved = false;
				var activeSection = $('.section.active');
				var scrollable;
				var xThreshold = 100;
				if (!isMoving) {
					touchEndY = e.touches[0].pageY;
					touchEndX = e.touches[0].pageX;
					if (activeSection.find('.slides').length && Math.abs(touchStartX - touchEndX) > (Math.abs(touchStartY - touchEndY) + xThreshold)) {
						if (touchStartX > touchEndX) {
							activeSection.find('.controlArrow.next').trigger('click');
						} else if (touchStartX < touchEndX) {
							activeSection.find('.controlArrow.prev').trigger('click');
						}
					} else {
						if (activeSection.find('.slides').length) {
							scrollable = activeSection.find('.slide.active').find('.scrollable');
						} else {
							scrollable = activeSection.find('.scrollable');
						}
						if (touchStartY > touchEndY) {
							if (scrollable.length > 0) {
								if (isScrolled('bottom', scrollable)) {
									$.fn.fullpage.moveSectionDown();
								} else {
									return true;
								}
							} else {
								$.fn.fullpage.moveSectionDown();
							}
						} else {
							if (scrollable.length > 0) {
								if (isScrolled('top', scrollable)) {
									$.fn.fullpage.moveSectionUp();
								} else {
									return true;
								}
							} else {
								$.fn.fullpage.moveSectionUp();
							}
						}
					}
				}
			}
		});
		$(document).on('touchstart', function(event) {
			if (options.autoScrolling && isTablet) {
				var e = event.originalEvent;
				touchStartY = e.touches[0].pageY;
				touchStartX = e.touches[0].pageX;
			}
		});
		function MouseWheelHandler(e) {
			if (options.autoScrolling) {
				e = window.event || e;
				var delta = Math.max(-1, Math.min(1, (e.wheelDelta || -e.detail)));
				var scrollable;
				var activeSection = $('.section.active');
				if (!isMoving) {
					if (activeSection.find('.slides').length) {
						scrollable = activeSection.find('.slide.active').find('.scrollable');
					} else {
						scrollable = activeSection.find('.scrollable');
					}
					if (delta < 0) {
						if (scrollable.length > 0) {
							if (isScrolled('bottom', scrollable)) {
								$.fn.fullpage.moveSectionDown();
							} else {
								return true;
							}
						} else {
							$.fn.fullpage.moveSectionDown();
						}
					} else {
						if (scrollable.length > 0) {
							if (isScrolled('top', scrollable)) {
								$.fn.fullpage.moveSectionUp();
							} else {
								return true;
							}
						} else {
							$.fn.fullpage.moveSectionUp();
						}
					}
				}
				return false;
			}
		}

		function addScrollEvent() {
			if (document.addEventListener) {
				document.addEventListener("mousewheel", MouseWheelHandler, false);
				document.addEventListener("DOMMouseScroll", MouseWheelHandler, false);
			} else {
				document.attachEvent("onmousewheel", MouseWheelHandler);
			}
		}
		$.fn.fullpage.moveSectionUp = function() {
			var prev = $('.section.active').prev('.section');
			if (options.loopTop && !prev.length) {
				prev = $('.section').last();
			}
			if (prev.length > 0 || (!prev.length && options.loopTop)) {
				scrollPage(prev);
			}
		};
		$.fn.fullpage.moveSectionDown = function() {
			var next = $('.section.active').next('.section');
			if (options.loopBottom && !next.length) {
				next = $('.section').first();
			}
			if (next.length > 0 || (!next.length && options.loopBottom)) {
				scrollPage(next);
			}
		};
		$.fn.fullpage.moveTo = function(section, slide) {
			var destiny = '';
			if (isNaN(section)) {
				destiny = $('[data-anchor="' + section + '"]');
			} else {
				destiny = $('.section').eq((section - 1));
			}
			if (slide !== 'undefined') {
				scrollPageAndSlide(section, slide);
			} else if (destiny.length > 0) {
				scrollPage(destiny);
			}
		};
		function scrollPage(element, callback) {
			var scrollOptions = {},
				scrolledElement;
			var dest = element.position();
			var dtop = dest !== null ? dest.top : null;
			var yMovement = getYmovement(element);
			var anchorLink = element.data('anchor');
			var sectionIndex = element.index('.section');
			var leavingSection = $('.section.active').index('.section') + 1;
			element.addClass('active').siblings().removeClass('active');
			isMoving = true;
			if (!$.isFunction(callback)) {
				if (typeof anchorLink !== 'undefined') {
					location.hash = anchorLink;
				} else {
					location.hash = '';
				}
			}
			if (options.autoScrolling) {
				scrollOptions['top'] = -dtop;
				scrolledElement = '#superContainer';
			} else {
				scrollOptions['scrollTop'] = dtop;
				scrolledElement = 'html, body';
			}
			if (options.css3 && options.autoScrolling) {
				$.isFunction(options.onLeave) && options.onLeave.call(this, leavingSection, yMovement);
				var translate3d = 'translate3d(0px, -' + dtop + 'px, 0px)';
				transformContainer(translate3d, true);
				setTimeout(function() {
					$.isFunction(options.afterLoad) && options.afterLoad.call(this, anchorLink, (sectionIndex + 1));
					setTimeout(function() {
						isMoving = false;
						$.isFunction(callback) && callback.call(this);
					}, scrollDelay);
				}, options.scrollingSpeed);
			} else {
				$.isFunction(options.onLeave) && options.onLeave.call(this, leavingSection, yMovement);
				$(scrolledElement).animate(scrollOptions, options.scrollingSpeed, options.easing, function() {
					$.isFunction(options.afterLoad) && options.afterLoad.call(this, anchorLink, (sectionIndex + 1));
					setTimeout(function() {
						isMoving = false;
						$.isFunction(callback) && callback.call(this);
					}, scrollDelay);
				});
			}
			lastScrolledDestiny = anchorLink;
			if (options.autoScrolling) {
				activateMenuElement(anchorLink);
				activateNavDots(anchorLink, sectionIndex);
			}
		}

		function scrollToAnchor() {
			var value = window.location.hash.replace('#', '').split('/');
			var section = value[0];
			var slide = value[1];
			if (section) {
				scrollPageAndSlide(section, slide);
			}
		}
		$(window).on('hashchange', function() {
			if (!isScrolling) {
				var value = window.location.hash.replace('#', '').split('/');
				var section = value[0];
				var slide = value[1];
				if (section && section !== lastScrolledDestiny || (typeof slide != 'undefined' && !slideMoving)) {
					scrollPageAndSlide(section, slide);
				}
			}
		});
		$(document).keydown(function(e) {
			if (!isMoving) {
				switch (e.which) {
				case 38:
				case 33:
					$.fn.fullpage.moveSectionUp();
					break;
				case 40:
				case 34:
					$.fn.fullpage.moveSectionDown();
					break;
				case 37:
					$('.section.active').find('.controlArrow.prev').trigger('click');
					break;
				case 39:
					$('.section.active').find('.controlArrow.next').trigger('click');
					break;
				default:
					return;
				}
			}
		});
		$(document).on('click', '#fullPage-nav a', function(e) {
			e.preventDefault();
			var index = $(this).parent().index();
			scrollPage($('.section').eq(index));
		});
		$(document).on({
			mouseenter: function() {
				var tooltip = $(this).data('tooltip');
				$('<div class="fullPage-tooltip ' + options.navigationPosition + '">' + tooltip + '</div>').hide().appendTo($(this)).fadeIn(200);
			},
			mouseleave: function() {
				$(this).find('.fullPage-tooltip').fadeOut().remove();
			}
		}, '#fullPage-nav li');
		if (options.normalScrollElements) {
			$(document).on('mouseover', options.normalScrollElements, function() {
				if (document.addEventListener) {
					document.removeEventListener('mousewheel', MouseWheelHandler, false);
					document.removeEventListener('DOMMouseScroll', MouseWheelHandler, false);
				} else {
					document.detachEvent("onmousewheel", MouseWheelHandler);
				}
			});
			$(document).on('mouseout', options.normalScrollElements, function() {
				addScrollEvent();
			});
		}
		$('.section').on('click', '.controlArrow', function() {
			if (slideMoving) {
				return;
			}
			slideMoving = true;
			var slides = $(this).closest('.section').find('.slides');
			var currentSlide = slides.find('.slide.active');
			var destiny = null;
			if ($(this).hasClass('prev')) {
				destiny = currentSlide.prev('.slide');
			} else {
				destiny = currentSlide.next('.slide');
			}
			if (!destiny.length) {
				if ($(this).hasClass('prev')) {
					destiny = currentSlide.siblings(':last');
				} else {
					destiny = currentSlide.siblings(':first');
				}
			}
			landscapeScroll(slides, destiny);
		});
		$('.section').on('click', '.toSlide', function(e) {
			e.preventDefault();
			var slides = $(this).closest('.section').find('.slides');
			var currentSlide = slides.find('.slide.active');
			var destiny = null;
			destiny = slides.find('.slide').eq(($(this).data('index') - 1));
			if (destiny.length > 0) {
				landscapeScroll(slides, destiny);
			}
		});
		function landscapeScroll(slides, destiny) {
			var destinyPos = destiny.position();
			var slidesContainer = slides.find('.slidesContainer').parent();
			var slideIndex = destiny.index();
			var section = slides.closest('.section');
			var sectionIndex = section.index('.section');
			var anchorLink = section.data('anchor');
			var slidesNav = section.find('.fullPage-slidesNav');
			var slideAnchor = destiny.data('anchor');
			if (options.onSlideLeave) {
				var prevSlideIndex = section.find('.slide.active').index();
				var xMovement = getXmovement(prevSlideIndex, slideIndex);
				$.isFunction(options.onSlideLeave) && options.onSlideLeave.call(this, anchorLink, (sectionIndex + 1), prevSlideIndex, xMovement);
			}
			destiny.addClass('active').siblings().removeClass('active');
			if (typeof slideAnchor === 'undefined') {
				slideAnchor = slideIndex;
			}
			if (section.hasClass('active')) {
				if (!options.loopHorizontal) {
					section.find('.controlArrow.prev').toggle(slideIndex != 0);
					section.find('.controlArrow.next').toggle(!destiny.is(':last-child'));
				}
				if (slideIndex) {
					if (typeof anchorLink !== 'undefined') {
						var sectionHash = anchorLink;
					} else {
						var sectionHash = '';
					}
					location.hash = sectionHash + '/' + slideAnchor;
				} else {
					location.hash = location.hash.split('/')[0];
				}
			}
			if (options.css3) {
				var translate3d = 'translate3d(-' + destinyPos.left + 'px, 0px, 0px)';
				slides.find('.slidesContainer').addClass('easing').css({
					'-webkit-transform': translate3d,
					'-moz-transform': translate3d,
					'-ms-transform': translate3d,
					'transform': translate3d
				});
				setTimeout(function() {
					$.isFunction(options.afterSlideLoad) && options.afterSlideLoad.call(this, anchorLink, (sectionIndex + 1), slideAnchor, slideIndex);
					slideMoving = false;
				}, options.scrollingSpeed);
			} else {
				slidesContainer.animate({
					scrollLeft: destinyPos.left
				}, options.scrollingSpeed, function() {
					$.isFunction(options.afterSlideLoad) && options.afterSlideLoad.call(this, anchorLink, (sectionIndex + 1), slideAnchor, slideIndex);
					slideMoving = false;
				});
			}
			slidesNav.find('.active').removeClass('active');
			slidesNav.find('li').eq(slideIndex).find('a').addClass('active');
		}
		if (!isTablet) {
			var resizeId;
			$(window).resize(function() {
				clearTimeout(resizeId);
				resizeId = setTimeout(doneResizing, 500);
			});
		}
		$(window).bind('orientationchange', function() {
			doneResizing();
		});
		function doneResizing() {
			var windowsWidth = $(window).width();
			windowsHeight = $(window).height();
			if (options.resize) {
				resizeMe(windowsHeight, windowsWidth);
			}
			$('.section').each(function() {
				var scrollHeight = windowsHeight - parseInt($(this).css('padding-bottom')) - parseInt($(this).css('padding-top'));
				if (options.scrollOverflow) {
					var slides = $(this).find('.slide');
					if (slides.length) {
						slides.each(function() {
							createSlimScrolling($(this));
						});
					} else {
						createSlimScrolling($(this));
					}
				}
				if (options.verticalCentered) {
					$(this).find('.tableCell').css('height', windowsHeight + 'px');
				}
				$(this).css('height', windowsHeight + 'px');
				var slides = $(this).find('.slides');
				if (slides.length) {
					landscapeScroll(slides, slides.find('.slide.active'));
				}
			});
			var destinyPos = $('.section.active').position();
			var activeSection = $('.section.active');
			if (activeSection.index('.section')) {
				scrollPage(activeSection);
			}
		}

		function resizeMe(displayHeight, displayWidth) {
			var preferredHeight = 825;
			var windowSize = displayHeight;
			if (displayHeight < 825 || displayWidth < 900) {
				if (displayWidth < 900) {
					windowSize = displayWidth;
					preferredHeight = 900;
				}
				var percentage = (windowSize * 100) / preferredHeight;
				var newFontSize = percentage.toFixed(2);
			}
		}

		function activateNavDots(name, sectionIndex) {
			if (options.navigation) {
				$('#fullPage-nav').find('.active').removeClass('active');
				if (name) {
					$('#fullPage-nav').find('a[href="#' + name + '"]').addClass('active');
				} else {
					$('#fullPage-nav').find('li').eq(sectionIndex).find('a').addClass('active');
				}
			}
		}

		function activateMenuElement(name) {
			if (options.menu) {
				$(options.menu).find('.active').removeClass('active');
				$(options.menu).find('[data-menuanchor="' + name + '"]').addClass('active');
			}
		}

		function isScrolled(type, scrollable) {
			if (type === 'top') {
				return !scrollable.scrollTop();
			} else if (type === 'bottom') {
				return scrollable.scrollTop() + scrollable.innerHeight() >= scrollable[0].scrollHeight;
			}
		}

		function getYmovement(destiny) {
			var fromIndex = $('.section.active').index('.section');
			var toIndex = destiny.index('.section');
			if (fromIndex > toIndex) {
				return 'up';
			}
			return 'down';
		}

		function getXmovement(fromIndex, toIndex) {
			if (fromIndex > toIndex) {
				return 'left';
			}
			return 'right';
		}

		function createSlimScrolling(element) {
			element.css('overflow', 'hidden');
			var section = element.closest('.section');
			var scrollable = element.find('.scrollable');
			if (scrollable.length) {
				var contentHeight = element.find('.scrollable').get(0).scrollHeight - parseInt(section.css('padding-bottom')) - parseInt(section.css('padding-top'));
			} else {
				var contentHeight = element.get(0).scrollHeight - parseInt(section.css('padding-bottom')) - parseInt(section.css('padding-top'));
			}
			if (contentHeight > windowsHeight) {
				var scrollHeight = windowsHeight - parseInt(section.css('padding-bottom')) - parseInt(section.css('padding-top'));
				if (scrollable.length) {
					scrollable.css('height', scrollHeight + 'px').parent().css('height', scrollHeight + 'px');
				} else {
					if (options.verticalCentered) {
						element.find('.tableCell').wrapInner('<div class="scrollable" />');
					} else {
						element.wrapInner('<div class="scrollable" />');
					}
					element.find('.scrollable').slimScroll({
						height: scrollHeight + 'px',
						size: '10px',
						alwaysVisible: true
					});
				}
			} else {
				element.find('.scrollable').children().first().unwrap().unwrap();
				element.find('.slimScrollBar').remove();
				element.find('.slimScrollRail').remove();
			}
			element.css('overflow', '');
		}

		function addTableClass(element) {
			element.addClass('table').wrapInner('<div class="tableCell" style="height:' + windowsHeight + 'px;" />');
		}

		function transformContainer(translate3d, animated) {
			$('#superContainer').toggleClass('easing', animated);
			$('#superContainer').css({
				'-webkit-transform': translate3d,
				'-moz-transform': translate3d,
				'-ms-transform': translate3d,
				'transform': translate3d
			});
		}

		function scrollPageAndSlide(destiny, slide) {
			if (isNaN(destiny)) {
				var section = $('[data-anchor="' + destiny + '"]');
			} else {
				var section = $('.section').eq((destiny - 1));
			}
			if (destiny !== lastScrolledDestiny) {
				scrollPage(section, function() {
					scrollSlider(section, slide)
				});
			} else {
				scrollSlider(section, slide);
			}
		}

		function scrollSlider(section, slide) {
			if (typeof slide != 'undefined') {
				var slides = section.find('.slides');
				var destiny = slides.find('[data-anchor="' + slide + '"]');
				if (!destiny.length) {
					destiny = slides.find('.slide').eq(slide);
				}
				landscapeScroll(slides, destiny);
			}
		}

		function addSlidesNavigation(section, numSlides) {
			section.append('<div class="fullPage-slidesNav"><ul></ul></div>');
			var nav = section.find('.fullPage-slidesNav');
			nav.addClass(options.slidesNavPosition);
			for (var i = 0; i < numSlides; i++) {
				nav.find('ul').append('<li><a href="#"><span></span></a></li>');
			}
			nav.css('margin-left', '-' + (nav.width() / 2) + 'px');
			nav.find('li').first().find('a').addClass('active');
		}
		$(document).on('click', '.fullPage-slidesNav a', function(e) {
			e.preventDefault();
			var slides = $(this).closest('.section').find('.slides');
			var destiny = slides.find('.slide').eq($(this).closest('li').index());
			landscapeScroll(slides, destiny);
		});
		function support3d() {
			var el = document.createElement('p'),
				has3d, transforms = {
					'webkitTransform': '-webkit-transform',
					'OTransform': '-o-transform',
					'msTransform': '-ms-transform',
					'MozTransform': '-moz-transform',
					'transform': 'transform'
				};
			document.body.insertBefore(el, null);
			for (var t in transforms) {
				if (el.style[t] !== undefined) {
					el.style[t] = "translate3d(1px,1px,1px)";
					has3d = window.getComputedStyle(el).getPropertyValue(transforms[t]);
				}
			}
			document.body.removeChild(el);
			return (has3d !== undefined && has3d.length > 0 && has3d !== "none");
		}
	};
})(jQuery);