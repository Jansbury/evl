
var $setTimeFade = 1000;


$(document).ready(function() {



	//full page set up
	$.fn.fullpage({
		verticalCentered: true,
		resize: true,
		anchors: ['welcome', 'offshoreselect', 'finance', 'contact'],
		scrollingSpeed: 200,
		easing: 'swing',
		menu: true,
		menu: '#menu',
		navigation: false,
		navigationPosition: 'right',
		slidesNavigation: false,
		slidesNavPosition: 'bottom',
		loopBottom: false,
		loopTop: false,
		loopHorizontal: false,
		autoScrolling:false,
		scrollOverflow: false,
		css3: true,
		normalScrollElements: 'iframe, .controlArrow',
		fixedElements: 'header, .headerBkg',
		touchSensitivity: 7,

		//ANIMATE CHARTS
		onSlideLeave: function(anchorLink, index, slideIndex, direction) {
			if (index == 2 && slideIndex == 1 && direction == 'right') {
			
			 //do something
				
			}
			
			if (index == 2 && slideIndex == 3 && direction == 'left') {
			
			 //do something
				
			}

			if (index == 3 && slideIndex == 1 && direction == 'right') {
			
			 //do something
			
			}
			
			if (index == 3 && slideIndex == 3 && direction == 'left') {
			
			 //do something
			 
			}
			
		},
		
				afterLoad: function(anchorLink, index){
						var $undLne = $('.mainUnderline'); 
					
            //using anchorLink
            if(anchorLink == 'welcome'){
                $undLne.css("width", "11%")
               
            }
            if(anchorLink == 'offshoreselect'){
                $undLne.css("width", "42%")
               
            }
            if(anchorLink == 'finance'){
                $undLne.css("width", "68%")
               
            }

            if(anchorLink == 'contact'){
                $undLne.css("width", "100%")
               
            }
        

        
						var currentIndex = -1,
							$olCp = $('ol.carProcess li div'),
							$olCpSpan = $('ol.carProcess li div p');
						$olCpSpan.hide();
						$($olCp).click(function() {
							"use strict";
							var selectedIndex = $olCp.index(this); /* 	$(this).toggleClass("rotate2"); */
							if (currentIndex !== selectedIndex) {
								$olCpSpan.eq(currentIndex).slideUp('1000', "swing").removeClass("active");
								$olCpSpan.eq(selectedIndex).slideDown('1000', "swing").addClass("active");
								currentIndex = selectedIndex;
							} else {
								$olCpSpan.eq(currentIndex).slideUp('1000', "swing").removeClass("active");
								currentIndex = -1;
							}
							
						});

						//auto FAQ hover
						var currentIndex = -1,
							$marker1 = $('.faqAuto ul li'),
							$boxTog1 = $('.faqAuto ul li span');
						$boxTog1.hide();
						$($marker1).click(function() {
							"use strict";
							var selectedIndex = $marker1.index(this); /* 	$(this).toggleClass("rotate2"); */
							if (currentIndex !== selectedIndex) {
								$boxTog1.eq(currentIndex).slideUp('1000', "swing").removeClass("active");
								$boxTog1.eq(selectedIndex).slideDown('1000', "swing").addClass("active");
								currentIndex = selectedIndex;
							} else {
								$boxTog1.eq(currentIndex).slideUp('1000', "swing").removeClass("active");
								currentIndex = -1;
							}	
						});
						
						//personal FAQ hover
						var currentIndex = -1,
							$marker2 = $('.faq ul li'),
							$boxTog2 = $('.faq ul li span');
						$boxTog2.hide();
						$($marker2).click(function() {
							"use strict";
							var selectedIndex = $marker2.index(this);
							if (currentIndex !== selectedIndex) {
								$boxTog2.eq(currentIndex).slideUp('1000', "swing").removeClass("active");
								$boxTog2.eq(selectedIndex).slideDown('1000', "swing").addClass("active");
								currentIndex = selectedIndex;
							} else {
								$boxTog2.eq(currentIndex).slideUp('1000', "swing").removeClass("active");
								currentIndex = -1;
							}
						});
					
						//dealer FAQ hover
						var currentIndex = -1,
							$marker = $('.dealerfaq ul li'),
							$boxTog = $('.dealerfaq ul li span');
						$boxTog.hide();
						$($marker).click(function() {
							"use strict";
							var selectedIndex = $marker.index(this);
							if (currentIndex !== selectedIndex) {
								$boxTog.eq(currentIndex).slideUp('1000', "swing").removeClass("active");
								$boxTog.eq(selectedIndex).slideDown('1000', "swing").addClass("active");
								currentIndex = selectedIndex;
							} else {
								$boxTog.eq(currentIndex).slideUp('1000', "swing").removeClass("active");
								currentIndex = -1;
							}
							
					
						});        
        }
	});
	
	//Section Scroll too links.
	/*
$('.contact-link').click(function() {
		$.fn.fullpage.moveTo('contact', 0);
	});
	
	$('.auto-link').click(function() {
		$.fn.fullpage.moveTo('carloan', 3);
	});
	
	$('.personal-link').click(function() {
		$.fn.fullpage.moveTo('personalloan', 3);
	});
	
	$('.register-link').click(function() {
		$.fn.fullpage.moveTo('dealerfinance', 2);
	});
	
	$('.about-link').click(function() {
		$.fn.fullpage.moveTo('welcome', 1);
	});
	$('.home-link').click(function() {
		$.fn.fullpage.moveTo('welcome', 0);
	});
*/
	
/*
						
	var map;
	var position = new google.maps.LatLng(-36.846450, 174.768787);
	var nzcgMap = 'custom_style';
	
	function initialize() {
		var featureOpts = [{
			"elementType": "geometry",
			"stylers": [{
				"lightness": 10
			}, {
				"weight": 2
			}, {
				"gamma": 0.78
			}, {
				"hue": "#fc0903"
			}, {
				"visibility": "simplified"
			}, {
				"saturation": 1
			}]
		}, {
			"elementType": "labels.text",
			"stylers": [{
				"color": "#000000"
			}, {
				"weight": 0.7
			}, {
				"visibility": "off"
			}]
		}, {
			"featureType": "road.local",
			"elementType": "labels.text.fill",
			"stylers": [{
				"visibility": "on"
			}, {
				"weight": 1
			}]
		}, {
			"featureType": "poi",
			"elementType": "all",
			"stylers": [{
				"visibility": "off"
			}]
		}, {
			"featureType": "water",
			"elementType": "geometry.fill",
			"stylers": [{
				"visibility": "on"
			}, {
				"color": "#00687d"
			}]
		}, {
			"featureType": "transit.station",
			"elementType": "labels",
			"stylers": [{
				"visibility": "off"
			}]
		}, {
			"featureType": "poi.park",
			"elementType": "geometry.fill",
			"stylers": [{
				"color": "#B3D5B7"
			}]
		}, {
			"featureType": "road",
			"elementType": "labels.icon",
			"stylers": [{
				"visibility": "off"
			}]
		}, {
			"featureType": "road.highway",
			"elementType": "geometry",
			"stylers": [{
				"visibility": "simplified"
			}, {
				"weight": 4
			}, {
				"saturation": -70
			}]
		}];
		var mapOptions = {
			zoom: 17,
			 scaleControl: false,
			center: position,
			mapTypeControlOptions: {
				mapTypeIds: [google.maps.MapTypeId.ROADMAP, nzcgMap]
			},
			mapTypeId: nzcgMap
		};
		map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
		var styledMapOptions = {
			name: ' '
		};
		var marker = new google.maps.Marker({
			position: position,
			map: map,
			title: 'NZCG'
		});
		var customMapType = new google.maps.StyledMapType(featureOpts, styledMapOptions);
		map.mapTypes.set(nzcgMap, customMapType);
		
		
	}
		google.maps.event.addDomListener(window, 'load', initialize);
*/



});//end doc-ready.

/*$(window).load(function(){


		setTimeout(function() {
						/* $('.preloader').fadeOut(1000); */
	/*					$('.preloader').animate({opacity: 0}, 1000);
					}, $setTimeFade);
			setTimeout(function() {		
						$('.preloader').hide();
	}, 3000);
	

 

});//end win-load.*/


	

var currentYear = (new Date).getFullYear();
$(document).ready(function() {
	$("#copyYear").text((new Date).getFullYear());
});

	//set variables references for all the various form elements;
	var $nameCon = $("#name"),
		$emailCon = $("#email"),
		$subjectCon = $("#subject"),
		$messageCon = $("#message"),
		$spamCon = $("#spam"),
		$formFieldsCon = $("input:text, textarea"),
		$statusCon = $("#status"),
		$resetBtn = $("input:reset"),
		$contactForm = $(".contact-form");
	//initialise
	$statusCon.hide(); /*submit handler for contact form*/
	$contactForm.submit(function(e) {
		//prevent default form submit action
		e.preventDefault();
		//clear all error borders from form fields
		$formFieldsCon.removeClass("error-focus");
		//check required fields are not empty and that the email address is valid
		if ($nameCon.val() == "") {
			setStatusMessageCon("Please Enter Your Name");
			$nameCon.setFocus();
		} else if ($emailCon.val() == "") {
			setStatusMessageCon("Please Enter Your Email Address");
			$emailCon.setFocus();
		} else if (!isValidEmailCon($emailCon.val())) {
			setStatusMessageCon("Please Enter a Valid Email Address");
			$emailCon.setFocus();
		} else if ($subjectCon.val() == "") {
			setStatusMessageCon("Please Enter a Subject");
			$subjectCon.setFocus();

		} else if ($messageCon.val() == "") {
			setStatusMessageCon("Please Enter Your Message");
			$messageCon.setFocus();
		} else if (!$spamCon.val() == "") {
			setStatusMessageCon("Spam Attack!!");
		} else {
			//if all fields are valid then send data to the server for processing and didplay "please wait" message
			setStatusMessageCon("Email being sent... please wait");
			//serialize all the form field values as a string
			var formDataCon = $(this).serialize();
			//send serialized data string to the send mail php via POST method
			$.post("mail-send.php", formDataCon, function(sent) {
				if (sent == "error") {
					setStatusMessageCon("Opps, something went wrong - please try again");
				} else if (sent == "success") {
					setStatusMessageCon("Thanks! " + $nameCon.val() + ", your message has been sent!");
					//clear form fields
					$formFieldsCon.val("");
				} //end if else
			}, "html");
		} //end else
	}); //end submit
	//click handler for reset button
	$resetBtn.click(function() {
		$statusCon.slideUp("fast");
		$formFieldsCon.removeClass("error-focus");
	});
	//helper functions

	function setStatusMessageCon(messageCon) {
		$statusCon.html(messageCon).slideDown("fast");
	}
	$.fn.setFocus = function() {
		return this.focus().addClass("error-focus");
	}

	function isValidEmailCon(emailCon) {
		var emailRx = /^[\w\.-]+@[\w\.-]+\.\w+$/;
		return emailRx.test(emailCon);
	}
	


	function isiPhone(){
	    return (
	        //Detect iPhone
	    //var isiPad = navigator.userAgent.match(/iPad/i) != null;
	        (navigator.platform.indexOf("iPhone") != -1) ||
	        //Detect iPod
	        (navigator.platform.indexOf("iPad") != -1)
	    );
	}


	if(isiPhone()){

		$('#auto').css('background-image','url(../i/autoBKG_s.jpg)');
		$('#personal').css('background-image','url(../i/personalBKG_s.jpg)');
		$('#dealer').css('background-image','url(../i/dealerBKG_s.jpg)');
		$('.homeImage').css('background-image','url(../i/homeBKG_s.jpg)');
		$('#about').css('background-image','url(../i/aboutBKG_s.jpg)');

}

	



