  $(".menu-open").click(function() {
  	var con = $('.menuConCon'),
  			$mS = 300, //menu animation speed
  			$mH = 210, //menu height
  			$mI = $('.menu-open'),
  			$mF = 200, //menu fade in.
  			menu	 = $('.menuCon');
  			
	  if(con.height() != $mH){	
	  	$mI.css('backgroundPosition', '0px -55px'),
			con.animate({height: $mH+"px"}, $mS, function(){
				menu.fadeTo($mF, 0.9);
				});
	  }else{
  		menu.fadeOut($mF, 0, function(){
				$mI.css('backgroundPosition', '0px 0px'),
				con.animate({height: "0px"}, $mS);
			});
		}
  });

			
	$(".scroll-to").click(function(event){		
		event.preventDefault();
		$('html,body').animate({scrollTop:$(this.hash).offset().top-55}, 700);
/*     history.pushState({}, "", this.href); */
	});
	
	
	
	//pop up open.
	var open = $('.applyClick'),
		 	pop = $('.popUp'),
		 	cntr = $('body'),
		 	cls	 = $('.close');
 
	 open.click(function(e){
    e.preventDefault(); 
    pop.fadeIn();
    cntr.css('overflow','hidden');
    cntr.bind('touchmove');
   });
   
   cls.click(function(e){
    e.preventDefault();
		cntr.css('overflow','auto');
		cntr.unbind('touchmove');
    pop.fadeOut();
   });   
   
   //logo to home;
   
   $('.logo').click(function () {
			$('body,html').animate({
				scrollTop: 0
			}, 600);
			return false;
		});
		
		
		//disable application submit untill all options yes.
/*
		$(":radio[name='player1rules']").click(function(){
		  var value = $(this).val();
		  if(value === "n"){
		    $("#join").attr("disabled", "disabled");
		    return;
		  }
		    $("#join").removeAttr("disabled");
		}).click()
*/
		
/*
	$('.onlineText').click(function() {
    var checked = $(".yandn :radio:checked");
    var groups = [];
    $(".yandn :radio").each(function() {
        if (groups.indexOf(this.name) < 0) {
            groups.push(this.name);
        }
    });
    if (groups.length == checked.length) {
        console.log('all are checked!');
				$('input[type="submit"]').removeAttr('disabled');
    }
    else {
        var total = groups.length - checked.length;
        console.log(total + ' group(s) are not selected');
        
    }
});
*/

$('input[type=date]').each(function(){
    var input=$(this);
    var id=input.attr('id');
    if (!id){
        id='RandomGeneratedId_'+Math.random().toString(36).substring(7);
        input.attr('id',id);
    }
    var placeholder=input.attr('placeholder');
    if (placeholder){
        $('head').append('<style> #'+id+':before{ content:"'+placeholder+'"; width:100%; color:#AAA; } #'+id+':focus:before,#'+id+'.not_empty:before{ content:none }</style>');
    }
    input.on('change keyup',function(){
        if ($(this).val()){
            $(this).addClass('not_empty')
        }else{
            $(this).removeClass('not_empty')
        }
        return false;
    });
});
		
		
		//application form js validation
		
		
		
	//set variables references for all the various form elements;
	var 
		$Fname = $("#d-Fname"),
		$Lname = $("#d-Lname"),
		$phone = $("#d-phone"),
		$email = $("#d-email"),
		$dob = $("#d-birth"),
		$borrow = $("#d-borrow"),
		$type = $("#q0_car"),
		$q1 = $("#q1_yes"),
		$q2 = $("#q2_yes"),
		$q3 = $("#q3_yes"),
		$spam = $("#spam"),
		$formFields = $("input:text, textarea"),
		$status = $("#status"),
		$resetBtn = $("input:reset"),
		$orderForm = $("#dealerForm");
	//initialise
	$status.hide(); /*submit handler for contact form*/
	$orderForm.submit(function(e) {
		//prevent default form submit action
		e.preventDefault();
		//clear all error borders from form fields
		$formFields.removeClass("error-focus");
		//check required fields are not empty and that the email address is valid
		if ($Fname.val() == "") {
			setStatusMessage("Please enter your first name");
			$Fname.setFocus();
		} else if ($Lname.val() == "") {
			setStatusMessage("Please enter your last name");
			$Lname.setFocus();
		} else if ($phone.val() == "") {
			setStatusMessage("Please enter your number");
			$phone.setFocus();	
		} else if ($email.val() == "") {
			setStatusMessage("Please Enter Your Email Address");
			$email.setFocus();
		} else if (!isValidEmail($email.val())) {
			setStatusMessage("Please enter a valid email address");
			$email.setFocus();
		} else if ($dob.val() == "") {
			setStatusMessage("Please your date of birth");
			$dob.setFocus();	
		} else if ($borrow.val() == "") {
			setStatusMessage("Please your date of birth");
			$borrow.setFocus();		
		
		
		} else if (!$('input[name=q0]:checked').val()) {
			setStatusMessage("Please select a Loan Type");
			$type.setFocus();	
			
		
		} else if (!$('input[name=q1]:checked').val()) {
			setStatusMessage("Are you over 18?");
			$q1.setFocus();	
		} else if ($('input[name=q1]:checked').val() === "No") {
			setStatusMessage("Sorry, you must be over 18 to apply for a loan.");
			$q1.setFocus();	
		
		} else if (!$('input[name=q2]:checked').val()) {
			setStatusMessage("Are you a NZ citizen or permanent resident?");
			$q2.setFocus();	
		} else if ($('input[name=q2]:checked').val() === "No" ) {
			setStatusMessage("Sorry, you must be a NZ citizen or permanent resident to apply for a loan.");
			$q2.setFocus();	
		
		} else if (!$('input[name=q3]:checked').val()) {
			setStatusMessage("Do you receive a regular income?");
			$q3.setFocus();	
		} else if ($('input[name=q3]:checked').val() === "No") {
			setStatusMessage("Sorry, you must receive a regular income to apply for a loan.");
			$q3.setFocus();	
		
		
		
		} else if (!$spam.val() == "") {
			setStatusMessage("Spam Attack!!");
		} else {
			//if all fields are valid then send data to the server for processing and didplay "please wait" message
			setStatusMessage("Email being sent... please wait");
			$('input:submit').attr("disabled", true);
			//serialize all the form field values as a string
			var formData = $(this).serialize();
			//send serialized data string to the send mail php via POST method
			
			$.post("https://nzcapitalgroup.co.nz/backoffice/index.php/MobileAppPage_Controller/Form", formData, function(sent) {
				console.log(sent);
				if (sent != "success") {
					setStatusMessage("Opps, something went wrong - please try again");
				} else {
					setStatusMessage("<span class='thank'> Thanks! " + $Fname.val() + ", your application has been submitted! One of our consultants will contact you within the hour. If you have any questions, or would like to discuss your application with us, please call <a href='tel:0800002844'>0800 002 844</a>.</span>");
					//clear form fields
					$formFields.val("");
				} //end if else
			}, "html");
  			
		} //end else
		
	}); //end submit
	//click handler for reset button
	$resetBtn.click(function() {
		$status.slideUp("fast");
		$formFields.removeClass("error-focus");
	});
	//helper functions

	function setStatusMessage(message) {
		$status.html(message).slideDown("fast");
	}
	$.fn.setFocus = function() {
		return this.focus().addClass("error-focus");
	}

	function isValidEmail(email) {
		var emailRx = /^[\w\.-]+@[\w\.-]+\.\w+$/;
		return emailRx.test(email);
	}
	
	
		
		
		

	 	