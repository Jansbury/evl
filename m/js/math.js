
//jQuery UI Touch Punch 0.2.3

!function(a){function f(a,b){if(!(a.originalEvent.touches.length>1)){a.preventDefault();var c=a.originalEvent.changedTouches[0],d=document.createEvent("MouseEvents");d.initMouseEvent(b,!0,!0,window,1,c.screenX,c.screenY,c.clientX,c.clientY,!1,!1,!1,!1,0,null),a.target.dispatchEvent(d)}}if(a.support.touch="ontouchend"in document,a.support.touch){var e,b=a.ui.mouse.prototype,c=b._mouseInit,d=b._mouseDestroy;b._touchStart=function(a){var b=this;!e&&b._mouseCapture(a.originalEvent.changedTouches[0])&&(e=!0,b._touchMoved=!1,f(a,"mouseover"),f(a,"mousemove"),f(a,"mousedown"))},b._touchMove=function(a){e&&(this._touchMoved=!0,f(a,"mousemove"))},b._touchEnd=function(a){e&&(f(a,"mouseup"),f(a,"mouseout"),this._touchMoved||f(a,"click"),e=!1)},b._mouseInit=function(){var b=this;b.element.bind({touchstart:a.proxy(b,"_touchStart"),touchmove:a.proxy(b,"_touchMove"),touchend:a.proxy(b,"_touchEnd")}),c.call(b)},b._mouseDestroy=function(){var b=this;b.element.unbind({touchstart:a.proxy(b,"_touchStart"),touchmove:a.proxy(b,"_touchMove"),touchend:a.proxy(b,"_touchEnd")}),d.call(b)}}}(jQuery);




$(function() {
     

	$("#slider-range-max").slider({
		range: "max",
		min: 1000,
		max: 30000,
		value: 2,
		step: 100,
		slide: function(event, ui) {

			
			var amount_with_percent = 0;
			
			var vdata = ui.value;
			

/* 				console.log(ui.value);		 */
			$("#amount1").val(ui.value);
			
			
			
			$("#amount").val( "$" + ui.value);			
			if ($("#loan_period1").val() == 0) $('#result').html('$0.00');
			else {
				if ($('#initr').is(':checked')) {
					var rate = 22.95;
					var amount = parseFloat(ui.value) + 601.95 + 495;
					var term = 52 * $("#year").val();
					var interest = (rate / 100) / 52;
					payment = 4.85 + (amount * interest) / (1 - Math.pow((1 + interest), -term));
					$('#result').html('$' + payment.toFixed(2));
				} else if ($('#ci').is(':checked')) {
					amount1 = parseFloat(ui.value) + parseFloat(ui.value * parseFloat($("#loan_period1").val()) / 100);
					amount = amount1 + 601.95;
					var term = 52 * $("#year").val();
					var rate = 19.95;
					var interest = (rate / 100) / 52;
					payment = (amount * interest) / (1 - Math.pow((1 + interest), -term));
					$('#result').html('$' + payment.toFixed(2));
				} else if ($('#gi').is(':checked')) {
					amount1 = parseFloat(ui.value);
					amount = amount1 + 601.95 + 350;
					var term = 52 * $("#year").val();
					var rate = 19.95;
					var interest = (rate / 100) / 52;
					payment = (amount * interest) / (1 - Math.pow((1 + interest), -term));
					$('#result').html('$' + payment.toFixed(2));
				} else if ($('#mw').is(':checked')) {
					amount = parseFloat(ui.value) + 1000 + 601.95;
					var term = 52 * $("#year").val();
					var rate = 19.95;
					var interest = (rate / 100) / 52;
					payment = (amount * interest) / (1 - Math.pow((1 + interest), -term));
					$('#result').html('$' + payment.toFixed(2));
				} else {
					amt = parseFloat(ui.value) + 601.95;
					var term = 52 * $("#year").val();
					var rate = 19.95;
					var interest = (rate / 100) / 52;
					payment = (amt * interest) / (1 - Math.pow((1 + interest), -term));
					$('#result').html('$' + payment.toFixed(2));
				}
				if ($('#ci').is(':checked') && $('#gi').is(':checked')) {
					amount1 = parseFloat(ui.value) + ((parseFloat(ui.value) * parseFloat($("#loan_period1").val()) / 100));
					amount = amount1 + 601.95 + 350;
					var term = 52 * $("#year").val();
					var rate = 19.95;
					var interest = (rate / 100) / 52;
					if (term != 0) {
						payment = (amount * interest) / (1 - Math.pow((1 + interest), -term));
						$('#result').html('$' + payment.toFixed(2));
					} else {
						$('#result').html('$0.00');
					}
				}
				if ($('#initr').is(':checked') && $('#ci').is(':checked')) {
					amount1 = parseFloat(ui.value) + ((parseFloat(ui.value) * parseFloat($("#loan_period1").val()) / 100));
					amount = amount1 + 601.95 + 495;
					var term = 52 * $("#year").val();
					var rate = 22.95;
					var interest = (rate / 100) / 52;
					if (term != 0) {
						payment = 4.99 + (amount * interest) / (1 - Math.pow((1 + interest), -term));
						$('#result').html('$' + payment.toFixed(2));
					} else {
						$('#result').html('$0.00');
					}
				}
				if ($('#initr').is(':checked') && $('#gi').is(':checked')) {
					amount = parseFloat(ui.value) + 601.95 + 495 + 350;
					var term = 52 * $("#year").val();
					var rate = 22.95;
					var interest = (rate / 100) / 52;
					if (term != 0) {
						payment = 4.99 + (amount * interest) / (1 - Math.pow((1 + interest), -term));
						$('#result').html('$' + payment.toFixed(2));
					} else {
						$('#result').html('$0.00');
					}
				}
				if ($('#initr').is(':checked') && $('#gi').is(':checked') && $('#ci').is(':checked')) {
					amount1 = parseFloat(ui.value) + ((parseFloat(ui.value) * parseFloat($("#loan_period1").val()) / 100));
					amount = amount1 + 601.95 + 495 + 350;
					var term = 52 * $("#year").val();
					var rate = 22.95;
					var interest = (rate / 100) / 52;
					if (term != 0) {
						payment = ((amount * interest) / (1 - Math.pow((1 + interest), -term)));
						payment1 = payment + 4.99;
						$('#result').html('$' + payment1.toFixed(2));
					} else {
						$('#result').html('$0.00');
					}
				}
				if ($('#initr').is(':checked') && $('#gi').is(':checked') && $('#ci').is(':checked') && $('#mw').is(':checked')) {
					amount1 = parseFloat(ui.value) + ((parseFloat(ui.value) * parseFloat($("#loan_period1").val()) / 100));
					amount = amount1 + 601.95 + 495 + 350 + 1000;
					var term = 52 * $("#year").val();
					var rate = 22.95;
					var interest = (rate / 100) / 52;
					if (term != 0) {
						payment = ((amount * interest) / (1 - Math.pow((1 + interest), -term)));
						payment1 = payment + 4.99;
						$('#result').html('$' + payment1.toFixed(2));
					} else {
						$('#result').html('$0.00');
					}
				}
			}
		}
	});
/* 	$('.ui-slider-handle').append('<span class="sidecar"><input type="text" id="amount" name="amount" readonly /></span>'); */
	$("#amount").val('$' + $("#slider-range-max").slider("value"));
	$("#amount1").val($("#slider-range-max").slider("value"));
});

function leaveChange() {
	if (document.getElementById("myList").value == "") {
		$("#loan_period1").val('0');
		$("#year").val('0');
	}
	if (document.getElementById("myList").value == "1") {
		$("#loan_period1").val('5.63');
		$("#year").val('1');
		yearChange(1);
	} else if (document.getElementById("myList").value == "2") {
		$("#loan_period1").val('10.63');
		$("#year").val('2');
		yearChange(2);
	} else if (document.getElementById("myList").value == "3") {
		$("#loan_period1").val('12.50');
		$("#year").val('3');
		yearChange(3);
	} else if (document.getElementById("myList").value == "4") {
		$("#loan_period1").val('14.00');
		$("#year").val('4');
		yearChange(4);
	}
}
var total = 0;
var sum = 0;
var i = 0;
$(function() {
	$("input[type=checkbox]").click(function() {
		if ($('#initr').is(':checked')) {
			var amount = parseFloat($("#amount1").val()) + 601.95 + 495;
			var term = 52 * $("#year").val();
			var rate = 22.95;
			var interest = (rate / 100) / 52;
			if (term != 0) {
				payment = 4.99 + (amount * interest) / (1 - Math.pow((1 + interest), -term));
				$('#result').html('$' + payment.toFixed(2));
			} else {
				$('#result').html('$0.00');
			}
		} else if ($('#ci').is(':checked')) {
			amount1 = parseFloat($("#amount1").val()) + ((parseFloat($("#amount1").val()) * parseFloat($("#loan_period1").val()) / 100));
			amount = amount1 + 601.95;
			var term = 52 * $("#year").val();
			var rate = 19.95;
			var interest = (rate / 100) / 52;
			if (term != 0) {
				payment = (amount * interest) / (1 - Math.pow((1 + interest), -term));
				$('#result').html('$' + payment.toFixed(2));
			} else {
				$('#result').html('$0.00');
			}
		} else if ($('#gi').is(':checked')) {
			amount1 = parseFloat($("#amount1").val());
			amount = amount1 + 601.95 + 350;
			var term = 52 * $("#year").val();
			var rate = 19.95;
			var interest = (rate / 100) / 52;
			if (term != 0) {
				payment = (amount * interest) / (1 - Math.pow((1 + interest), -term));
				$('#result').html('$' + payment.toFixed(2));
			} else {
				$('#result').html('$0.00');
			}
		} else if ($('#mw').is(':checked')) {
			amount = parseFloat($("#amount1").val()) + 1000 + 601.95;
			var term = 52 * $("#year").val();
			var rate = 19.95;
			var interest = (rate / 100) / 52;
			if (term != 0) {
				payment = (amount * interest) / (1 - Math.pow((1 + interest), -term));
				$('#result').html('$' + payment.toFixed(2));
			} else {
				$('#result').html('$0.00');
			}
		} else {
			amt = parseFloat($('#amount1').val()) + 601.95;
			var term = 52 * $("#year").val();
			var rate = 19.95;
			var interest = (rate / 100) / 52;
			payment = (amt * interest) / (1 - Math.pow((1 + interest), -term));
			$('#result').html('$' + payment.toFixed(2));
		}
		if ($('#ci').is(':checked') && $('#gi').is(':checked')) {
			amount1 = parseFloat($("#amount1").val()) + ((parseFloat($("#amount1").val()) * parseFloat($("#loan_period1").val()) / 100));
			amount = amount1 + 601.95 + 350;
			var term = 52 * $("#year").val();
			var rate = 19.95;
			var interest = (rate / 100) / 52;
			if (term != 0) {
				payment = (amount * interest) / (1 - Math.pow((1 + interest), -term));
				$('#result').html('$' + payment.toFixed(2));
			} else {
				$('#result').html('$0.00');
			}
		}
		if ($('#initr').is(':checked') && $('#ci').is(':checked')) {
			amount1 = parseFloat($("#amount1").val()) + ((parseFloat($("#amount1").val()) * parseFloat($("#loan_period1").val()) / 100));
			amount = amount1 + 601.95 + 495;
			var term = 52 * $("#year").val();
			var rate = 22.95;
			var interest = (rate / 100) / 52;
			if (term != 0) {
				payment = 4.99 + (amount * interest) / (1 - Math.pow((1 + interest), -term));
				$('#result').html('$' + payment.toFixed(2));
			} else {
				$('#result').html('$0.00');
			}
		}
		if ($('#initr').is(':checked') && $('#gi').is(':checked')) {
			amount = parseFloat($("#amount1").val()) + 601.95 + 495 + 350;
			var term = 52 * $("#year").val();
			var rate = 22.95;
			var interest = (rate / 100) / 52;
			if (term != 0) {
				payment = 4.99 + (amount * interest) / (1 - Math.pow((1 + interest), -term));
				$('#result').html('$' + payment.toFixed(2));
			} else {
				$('#result').html('$0.00');
			}
		}
		if ($('#initr').is(':checked') && $('#gi').is(':checked') && $('#ci').is(':checked')) {
			amount1 = parseFloat($("#amount1").val()) + ((parseFloat($("#amount1").val()) * parseFloat($("#loan_period1").val()) / 100));
			amount = amount1 + 601.95 + 495 + 350;
			var term = 52 * $("#year").val();
			var rate = 22.95;
			var interest = (rate / 100) / 52;
			if (term != 0) {
				payment = ((amount * interest) / (1 - Math.pow((1 + interest), -term)));
				payment1 = payment + 4.99;
				$('#result').html('$' + payment1.toFixed(2));
			} else {
				$('#result').html('$0.00');
			}
		}
		if ($('#initr').is(':checked') && $('#gi').is(':checked') && $('#ci').is(':checked') && $('#mw').is(':checked')) {
			amount1 = parseFloat($("#amount1").val()) + ((parseFloat($("#amount1").val()) * parseFloat($("#loan_period1").val()) / 100));
			amount = amount1 + 601.95 + 495 + 350 + 1000;
			var term = 52 * $("#year").val();
			var rate = 22.95;
			var interest = (rate / 100) / 52;
			if (term != 0) {
				payment = ((amount * interest) / (1 - Math.pow((1 + interest), -term)));
				payment1 = payment + 4.99;
				$('#result').html('$' + payment1.toFixed(2));
			} else {
				$('#result').html('$0.00');
			}
		}
	});
});
function personalloan() {
	if (document.getElementById("p_loan").value == "") {
		$("#loan_period2").val('0');
		$("#year1").val('0');
	}
	if (document.getElementById("p_loan").value == "1") {
		$("#loan_period2").val('5.63');
		$("#year1").val('1');
		yearChange2(1);
	} else if (document.getElementById("p_loan").value == "2") {
		$("#loan_period2").val('10.63');
		$("#year1").val('2');
		yearChange2(2);
	} else if (document.getElementById("p_loan").value == "3") {
		$("#loan_period2").val('12.50');
		$("#year1").val('3');
		yearChange2(3);
	} else if (document.getElementById("p_loan").value == "4") {
		$("#loan_period2").val('14.00');
		$("#year1").val('4');
		yearChange2(4);
	} else if (document.getElementById("p_loan").value == "5") {
		$("#loan_period2").val('14.00');
		$("#year1").val('5');
		yearChange2(5);
	}
}
var total = 0;
var sum = 0;
var i = 0;
$(function() {
	$("input[type=checkbox]").click(function() {
		if ($('#p_ci').is(':checked')) {
			amount1 = parseFloat($("#amount2").val()) + ((parseFloat($("#amount2").val()) * parseFloat($("#loan_period2").val()) / 100));
			amount = amount1 + 601.95;
			var term = 52 * $("#year1").val();
			var rate = 19.95;
			var interest = (rate / 100) / 52;
			if (term != 0) {
				payment = (amount * interest) / (1 - Math.pow((1 + interest), -term));
				$('#p_result').html('$' + payment.toFixed(2));
			} else {
				$('#p_result').html('$0.00');
			}
		} else {
			amount1 = parseFloat($("#amount2").val());
			amount = amount1 + 601.95;
			var term = 52 * $("#year1").val();
			var rate = 19.95;
			var interest = (rate / 100) / 52;
			if (term != 0) {
				payment = (amount * interest) / (1 - Math.pow((1 + interest), -term));
				$('#p_result').html('$' + payment.toFixed(2));
			} else {
				$('#p_result').html('$0.00');
			}
		}
	});
});


function yearChange(year)
{if($('#initr').is(':checked')){amount=parseFloat($("#amount1").val())+601.95+495;var rate=22.95;var term=52*year;var interest=(rate/100)/52;payment=4.99+(amount*interest)/(1-Math.pow((1+interest),-term));$('#result').html('$'+payment.toFixed(2));}
if($('#ci').is(':checked')){amount1=parseFloat($("#amount1").val())+parseFloat($("#amount1").val()*parseFloat($("#loan_period1").val())/100);amount=amount1+601.95;var term=52*year;var rate=19.95;var interest=(rate/100)/52;payment=(amount*interest)/(1-Math.pow((1+interest),-term));$('#result').html('$'+payment.toFixed(2));}
if($('#gi').is(':checked')){amount1=parseFloat($("#amount1").val());amount=amount1+601.95+350;var term=52*year;var rate=19.95;var interest=(rate/100)/52;payment=(amount*interest)/(1-Math.pow((1+interest),-term));$('#result').html('$'+payment.toFixed(2));}
if($('#mw').is(':checked')){amount=parseFloat($("#amount1").val())+1000+601.95;var term=52*year;var rate=19.95;var interest=(rate/100)/52;payment=(amount*interest)/(1-Math.pow((1+interest),-term));$('#result').html('$'+payment.toFixed(2));}
if($('#ci').is(':checked')&&$('#gi').is(':checked')&&!$('#initr').is(':checked'))
{amount1=parseFloat($("#amount1").val())+((parseFloat($("#amount1").val())*parseFloat($("#loan_period1").val())/100));amount=amount1+601.95+350;var term=52*year;var rate=19.95;var interest=(rate/100)/52;if(term!=0){payment=(amount*interest)/(1-Math.pow((1+interest),-term));$('#result').html('$'+payment.toFixed(2));}
else
{$('#result').html('$0.00');}}
if($('#initr').is(':checked')&&$('#ci').is(':checked')&&!$('#gi').is(':checked'))
{amount1=parseFloat($("#amount1").val())+((parseFloat($("#amount1").val())*parseFloat($("#loan_period1").val())/100));amount=amount1+601.95+495;var term=52*year;var rate=22.95;var interest=(rate/100)/52;if(term!=0){payment=4.99+(amount*interest)/(1-Math.pow((1+interest),-term));$('#result').html('$'+payment.toFixed(2));}
else
{$('#result').html('$0.00');}}
if($('#initr').is(':checked')&&$('#gi').is(':checked')&&!$('#ci').is(':checked'))
{amount=parseFloat($("#amount1").val())+601.95+495+350;var term=52*year;var rate=22.95;var interest=(rate/100)/52;if(term!=0){payment=4.99+(amount*interest)/(1-Math.pow((1+interest),-term));$('#result').html('$'+payment.toFixed(2));}
else
{$('#result').html('$0.00');}}
if($('#initr').is(':checked')&&$('#gi').is(':checked')&&$('#ci').is(':checked')&&!$('#mw').is(':checked'))
{amount1=parseFloat($("#amount1").val())+((parseFloat($("#amount1").val())*parseFloat($("#loan_period1").val())/100));amount=amount1+601.95+495+350;var term=52*year;var rate=22.95;var interest=(rate/100)/52;if(term!=0){payment=((amount*interest)/(1-Math.pow((1+interest),-term)));payment1=payment+4.99;$('#result').html('$'+payment1.toFixed(2));}
else
{$('#result').html('$0.00');}}
if($('#initr').is(':checked')&&$('#gi').is(':checked')&&$('#ci').is(':checked')&&$('#mw').is(':checked'))
{amount1=parseFloat($("#amount1").val())+((parseFloat($("#amount1").val())*parseFloat($("#loan_period1").val())/100));amount=amount1+601.95+495+350+1000;var term=52*year;var rate=22.95;var interest=(rate/100)/52;if(term!=0){payment=((amount*interest)/(1-Math.pow((1+interest),-term)));payment1=payment+4.99;$('#result').html('$'+payment1.toFixed(2));}
else
{$('#result').html('$0.00');}}
if(!$('#initr').is(':checked')&&!$('#gi').is(':checked')&&!$('#ci').is(':checked')&&!$('#mw').is(':checked'))
{amount=parseFloat($("#amount1").val())+601.95;var rate=19.95;var term=52*year;var interest=(rate/100)/52;payment=(amount*interest)/(1-Math.pow((1+interest),-term));$('#result').html('$'+payment.toFixed(2));}}
function yearChange2(year)
{if($('#p_ci').is(':checked')){amount1=parseFloat($("#amount2").val())+parseFloat($("#amount2").val()*parseFloat($("#loan_period2").val())/100);amount=amount1+601.95;var term=52*year;var rate=19.95;var interest=(rate/100)/52;payment=(amount*interest)/(1-Math.pow((1+interest),-term));$('#p_result').html('$'+payment.toFixed(2));}
else
{amount1=parseFloat($("#amount2").val());amount=amount1+601.95;var term=52*year;var rate=19.95;var interest=(rate/100)/52;payment=(amount*interest)/(1-Math.pow((1+interest),-term));$('#p_result').html('$'+payment.toFixed(2));}}
function yearChange3(year){if($('#pc_ci').is(':checked')){amount1=parseFloat($("#amount3").val())+parseFloat($("#amount3").val()*parseFloat($("#loan_period3").val())/100);amount=amount1+896.95;var term=52*year;var rate=17.95;var interest=(rate/100)/52;payment=(amount*interest)/(1-Math.pow((1+interest),-term));$('#pc_result').html('$'+payment.toFixed(2));}
else
{amount1=parseFloat($("#amount3").val());amount=amount1+896.95;var term=52*year;var rate=17.95;var interest=(rate/100)/52;payment=(amount*interest)/(1-Math.pow((1+interest),-term));$('#pc_result').html('$'+payment.toFixed(2));}}
function yearChange4(year){if($('#move_smart').is(':checked')){console.log(parseFloat($("#amount4").val())*parseFloat($("#loan_period4").val()/100));amount1=parseFloat($("#amount4").val())+(parseFloat($("#amount4").val())*parseFloat($("#loan_period4").val()/100));amount=amount1+326.95;var term=52*year/12;var rate=24.95;var interest=(rate/100)/52;payment=(amount*interest)/(1-Math.pow((1+interest),-term));$('#move_result').html('$'+payment.toFixed(2));}}

