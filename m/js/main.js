
var $setTimeFade = 1000;


$(document).ready(function() {



	//full page set up
	$.fn.fullpage({
		verticalCentered: true,
		resize: true,
		anchors: ['welcome', 'carloan', 'personalloan', 'dealerfinance', 'contact'],
		scrollingSpeed: 200,
		easing: 'swing',
		menu: true,
		menu: '#menu',
		navigation: false,
		navigationPosition: 'right',
		slidesNavigation: false,
		slidesNavPosition: 'bottom',
		loopBottom: false,
		loopTop: false,
		loopHorizontal: false,
		autoScrolling:true,
		scrollOverflow: false,
		css3: true,
		normalScrollElements: 'iframe, .controlArrow, .col-md-9',
		fixedElements: 'header, .headerBkg',
		touchSensitivity: 7,

		//ANIMATE CHARTS
		onSlideLeave: function(anchorLink, index, slideIndex, direction) {
			if (index == 2 && slideIndex == 1 && direction == 'right') {
			
			 setTimeout(function() {	
					var $cc1 = $('.infoOne-1'), $cc2 = $('.infoOne-2'), $cc3 = $('.infoOne-3'), $cc4 = $('.infoOne-4'),
							$cc5 = $('.infoOne-5'), $cc6 = $('.infoOne-6'), $cc7 = $('.infoOne-7');
					$($cc1).animate({
						height: '50px'
					}), setTimeout(function() {
						$($cc2).animate({ height: '156px' })
					}, 200);
					setTimeout(function() {
						$($cc3).animate({ height: '50px' })
					}, 100);
					setTimeout(function() {
						$($cc4).animate({ height: '102px' })
					}, 200);
					setTimeout(function() {
						$($cc5).animate({ height: '50px' })
					}, 300);
					setTimeout(function() {
						$($cc6).animate({ height: '145px' })
					}, 100);
					setTimeout(function() {
						$($cc7).animate({ height: '61px' })
					}, 400);
				});//end containing timeout
				
			}
			
			if (index == 2 && slideIndex == 3 && direction == 'left') {
			
			 setTimeout(function() {	
					var $cc1 = $('.infoOne-1'), $cc2 = $('.infoOne-2'), $cc3 = $('.infoOne-3'), $cc4 = $('.infoOne-4'),
							$cc5 = $('.infoOne-5'), $cc6 = $('.infoOne-6'), $cc7 = $('.infoOne-7');
					$($cc1).animate({
						height: '50px'
					}), setTimeout(function() {
						$($cc2).animate({ height: '156px' })
					}, 200);
					setTimeout(function() {
						$($cc3).animate({ height: '50px' })
					}, 100);
					setTimeout(function() {
						$($cc4).animate({ height: '102px' })
					}, 200);
					setTimeout(function() {
						$($cc5).animate({ height: '50px' })
					}, 300);
					setTimeout(function() {
						$($cc6).animate({ height: '145px' })
					}, 100);
					setTimeout(function() {
						$($cc7).animate({ height: '61px' })
					}, 400);
				});//end containing timeout
				
			}

			if (index == 3 && slideIndex == 1 && direction == 'right') {
			
			 setTimeout(function() {	
					var $ch1 = $('.infoTwo-1'),
							$ch2 = $('.infoTwo-2'),
							$ch3 = $('.infoTwo-3'),
							$ch4 = $('.infoTwo-4'),
							$ch5 = $('.infoTwo-5'),
							$ch6 = $('.infoTwo-6'),
							$ch7 = $('.infoTwo-7');
					$($ch1).animate({
						height: '50px'
					}), setTimeout(function() {
						$($ch2).animate({ height: '156px' })
					}, 200);
					setTimeout(function() {
						$($ch3).animate({ height: '50px' })
					}, 100);
					setTimeout(function() {
						$($ch4).animate({ height: '102px' });
					}, 200);
					setTimeout(function() {
						$($ch5).animate({ height: '50px' });
					}, 300);
					setTimeout(function() {
						$($ch6).animate({ height: '145px' });
					}, 100);
					setTimeout(function() {
						$($ch7).animate({ height: '61px' });
					}, 400);

				});//end containing timeout
			}
			
			if (index == 3 && slideIndex == 3 && direction == 'left') {
			
			 setTimeout(function() {	
					var $ch1 = $('.infoTwo-1'),
							$ch2 = $('.infoTwo-2'),
							$ch3 = $('.infoTwo-3'),
							$ch4 = $('.infoTwo-4'),
							$ch5 = $('.infoTwo-5'),
							$ch6 = $('.infoTwo-6'),
							$ch7 = $('.infoTwo-7');
					$($ch1).animate({
						height: '50px'
					}), setTimeout(function() {
						$($ch2).animate({ height: '156px' })
					}, 200);
					setTimeout(function() {
						$($ch3).animate({ height: '50px' })
					}, 100);
					setTimeout(function() {
						$($ch4).animate({ height: '102px' });
					}, 200);
					setTimeout(function() {
						$($ch5).animate({ height: '50px' });
					}, 300);
					setTimeout(function() {
						$($ch6).animate({ height: '145px' });
					}, 100);
					setTimeout(function() {
						$($ch7).animate({ height: '61px' });
					}, 400);

				});//end containing timeout
			}
			
		},
		
				afterLoad: function(anchorLink, index){
						var $undLne = $('.mainUnderline'); 
					
            //using anchorLink
            if(anchorLink == 'welcome'){
                $undLne.css("width", "8%")
               
            }
            if(anchorLink == 'carloan'){
                $undLne.css("width", "26%")
               
            }
            if(anchorLink == 'personalloan'){
                $undLne.css("width", "47%")
               
            }
						if(anchorLink == 'dealerfinance'){
                $undLne.css("width", "71%")
               
            }
            if(anchorLink == 'contact'){
                $undLne.css("width", "92%")
               
            }
        

        
						var currentIndex = -1,
							$olCp = $('ol.carProcess li div'),
							$olCpSpan = $('ol.carProcess li div p');
						$olCpSpan.hide();
						$($olCp).click(function() {
							"use strict";
							var selectedIndex = $olCp.index(this); /* 	$(this).toggleClass("rotate2"); */
							if (currentIndex !== selectedIndex) {
								$olCpSpan.eq(currentIndex).slideUp('1000', "swing").removeClass("active");
								$olCpSpan.eq(selectedIndex).slideDown('1000', "swing").addClass("active");
								currentIndex = selectedIndex;
							} else {
								$olCpSpan.eq(currentIndex).slideUp('1000', "swing").removeClass("active");
								currentIndex = -1;
							}
							
						});

						//auto FAQ hover
						var currentIndex = -1,
							$marker1 = $('.faqAuto ul li'),
							$boxTog1 = $('.faqAuto ul li span');
						$boxTog1.hide();
						$($marker1).click(function() {
							"use strict";
							var selectedIndex = $marker1.index(this); /* 	$(this).toggleClass("rotate2"); */
							if (currentIndex !== selectedIndex) {
								$boxTog1.eq(currentIndex).slideUp('1000', "swing").removeClass("active");
								$boxTog1.eq(selectedIndex).slideDown('1000', "swing").addClass("active");
								currentIndex = selectedIndex;
							} else {
								$boxTog1.eq(currentIndex).slideUp('1000', "swing").removeClass("active");
								currentIndex = -1;
							}	
						});
						
						//personal FAQ hover
						var currentIndex = -1,
							$marker2 = $('.faq ul li'),
							$boxTog2 = $('.faq ul li span');
						$boxTog2.hide();
						$($marker2).click(function() {
							"use strict";
							var selectedIndex = $marker2.index(this);
							if (currentIndex !== selectedIndex) {
								$boxTog2.eq(currentIndex).slideUp('1000', "swing").removeClass("active");
								$boxTog2.eq(selectedIndex).slideDown('1000', "swing").addClass("active");
								currentIndex = selectedIndex;
							} else {
								$boxTog2.eq(currentIndex).slideUp('1000', "swing").removeClass("active");
								currentIndex = -1;
							}
						});
					
						//dealer FAQ hover
						var currentIndex = -1,
							$marker = $('.dealerfaq ul li'),
							$boxTog = $('.dealerfaq ul li span');
						$boxTog.hide();
						$($marker).click(function() {
							"use strict";
							var selectedIndex = $marker.index(this);
							if (currentIndex !== selectedIndex) {
								$boxTog.eq(currentIndex).slideUp('1000', "swing").removeClass("active");
								$boxTog.eq(selectedIndex).slideDown('1000', "swing").addClass("active");
								currentIndex = selectedIndex;
							} else {
								$boxTog.eq(currentIndex).slideUp('1000', "swing").removeClass("active");
								currentIndex = -1;
							}
							
					
						});        
        }
	});
	
	//Section Scroll too links.
	$('.contact-link').click(function() {
		$.fn.fullpage.moveTo('contact', 0);
	});
	
	$('.auto-link').click(function() {
		$.fn.fullpage.moveTo('carloan', 3);
	});
	
	$('.personal-link').click(function() {
		$.fn.fullpage.moveTo('personalloan', 3);
	});
	
	$('.register-link').click(function() {
		$.fn.fullpage.moveTo('dealerfinance', 2);
	});
	
	$('.about-link').click(function() {
		$.fn.fullpage.moveTo('welcome', 1);
	});
	$('.home-link').click(function() {
		$.fn.fullpage.moveTo('welcome', 0);
	});
	
						
	var map;
	var position = new google.maps.LatLng(-36.846450, 174.768787);
	var nzcgMap = 'custom_style';
	
	function initialize() {
		var featureOpts = [{
			"elementType": "geometry",
			"stylers": [{
				"lightness": 10
			}, {
				"weight": 2
			}, {
				"gamma": 0.78
			}, {
				"hue": "#009ebc"
			}, {
				"visibility": "simplified"
			}, {
				"saturation": 1
			}]
		}, {
			"elementType": "labels.text",
			"stylers": [{
				"color": "#000000"
			}, {
				"weight": 0.7
			}, {
				"visibility": "off"
			}]
		}, {
			"featureType": "road.local",
			"elementType": "labels.text.fill",
			"stylers": [{
				"visibility": "on"
			}, {
				"weight": 1
			}]
		}, {
			"featureType": "poi",
			"elementType": "all",
			"stylers": [{
				"visibility": "off"
			}]
		}, {
			"featureType": "water",
			"elementType": "geometry.fill",
			"stylers": [{
				"visibility": "on"
			}, {
				"color": "#00687d"
			}]
		}, {
			"featureType": "transit.station",
			"elementType": "labels",
			"stylers": [{
				"visibility": "off"
			}]
		}, {
			"featureType": "poi.park",
			"elementType": "geometry.fill",
			"stylers": [{
				"color": "#B3D5B7"
			}]
		}, {
			"featureType": "road",
			"elementType": "labels.icon",
			"stylers": [{
				"visibility": "off"
			}]
		}, {
			"featureType": "road.highway",
			"elementType": "geometry",
			"stylers": [{
				"visibility": "simplified"
			}, {
				"weight": 4
			}, {
				"saturation": -70
			}]
		}];
		var mapOptions = {
			zoom: 17,
			 scaleControl: false,
			center: position,
			mapTypeControlOptions: {
				mapTypeIds: [google.maps.MapTypeId.ROADMAP, nzcgMap]
			},
			mapTypeId: nzcgMap
		};
		map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
		var styledMapOptions = {
			name: ' '
		};
		var marker = new google.maps.Marker({
			position: position,
			map: map,
			title: 'NZCG'
		});
		var customMapType = new google.maps.StyledMapType(featureOpts, styledMapOptions);
		map.mapTypes.set(nzcgMap, customMapType);
		
		
	}
		google.maps.event.addDomListener(window, 'load', initialize);



});//end doc-ready.

$(window).load(function(){


		setTimeout(function() {
						/* $('.preloader').fadeOut(1000); */
						$('.preloader').animate({opacity: 0}, 1000);
					}, $setTimeFade);
			setTimeout(function() {		
						$('.preloader').hide();
	}, 3000);
	

	$("#iframeAuto").each(function() {
		var url = "http://nzcapitalgroup.co.nz/backoffice/MSAP";
	$(this).replaceWith('<iframe src=' + url + ' id="iframeAuto" class="applyForm onlite"></iframe>');
 });
 
 	$(".applyForm").each(function() {
		var url = "http://nzcapitalgroup.co.nz/backoffice/MSAP";
	$(this).replaceWith('<iframe src=' + url + ' id="iframeAuto" class="applyForm onlite"></iframe>');
 });
 
 

});//end win-load.


	
$(function(){$("#slider-range-max").slider({range:"max",min:1000,max:30000,value:2,step:100,slide:function(event,ui){var amount_with_percent=0;var vdata=ui.value;$("#amount1").val(ui.value);$("#amount").val("$"+ui.value);if($("#loan_period1").val()==0)$('#result').html('$0.00');else{if($('#initr').is(':checked')){var rate=22.95;var amount=parseFloat(ui.value)+601.95+495;var term=52*$("#year").val();var interest=(rate/100)/52;payment=4.85+(amount*interest)/(1-Math.pow((1+interest),-term));$('#result').html('$'+payment.toFixed(2));}else if($('#ci').is(':checked')){amount1=parseFloat(ui.value)+parseFloat(ui.value*parseFloat($("#loan_period1").val())/100);amount=amount1+601.95;var term=52*$("#year").val();var rate=19.95;var interest=(rate/100)/52;payment=(amount*interest)/(1-Math.pow((1+interest),-term));$('#result').html('$'+payment.toFixed(2));}else if($('#gi').is(':checked')){amount1=parseFloat(ui.value);amount=amount1+601.95+350;var term=52*$("#year").val();var rate=19.95;var interest=(rate/100)/52;payment=(amount*interest)/(1-Math.pow((1+interest),-term));$('#result').html('$'+payment.toFixed(2));}else if($('#mw').is(':checked')){amount=parseFloat(ui.value)+1000+601.95;var term=52*$("#year").val();var rate=19.95;var interest=(rate/100)/52;payment=(amount*interest)/(1-Math.pow((1+interest),-term));$('#result').html('$'+payment.toFixed(2));}else{amt=parseFloat(ui.value)+601.95;var term=52*$("#year").val();var rate=19.95;var interest=(rate/100)/52;payment=(amt*interest)/(1-Math.pow((1+interest),-term));$('#result').html('$'+payment.toFixed(2));}
if($('#ci').is(':checked')&&$('#gi').is(':checked')){amount1=parseFloat(ui.value)+((parseFloat(ui.value)*parseFloat($("#loan_period1").val())/100));amount=amount1+601.95+350;var term=52*$("#year").val();var rate=19.95;var interest=(rate/100)/52;if(term!=0){payment=(amount*interest)/(1-Math.pow((1+interest),-term));$('#result').html('$'+payment.toFixed(2));}else{$('#result').html('$0.00');}}
if($('#initr').is(':checked')&&$('#ci').is(':checked')){amount1=parseFloat(ui.value)+((parseFloat(ui.value)*parseFloat($("#loan_period1").val())/100));amount=amount1+601.95+495;var term=52*$("#year").val();var rate=22.95;var interest=(rate/100)/52;if(term!=0){payment=4.99+(amount*interest)/(1-Math.pow((1+interest),-term));$('#result').html('$'+payment.toFixed(2));}else{$('#result').html('$0.00');}}
if($('#initr').is(':checked')&&$('#gi').is(':checked')){amount=parseFloat(ui.value)+601.95+495+350;var term=52*$("#year").val();var rate=22.95;var interest=(rate/100)/52;if(term!=0){payment=4.99+(amount*interest)/(1-Math.pow((1+interest),-term));$('#result').html('$'+payment.toFixed(2));}else{$('#result').html('$0.00');}}
if($('#initr').is(':checked')&&$('#gi').is(':checked')&&$('#ci').is(':checked')){amount1=parseFloat(ui.value)+((parseFloat(ui.value)*parseFloat($("#loan_period1").val())/100));amount=amount1+601.95+495+350;var term=52*$("#year").val();var rate=22.95;var interest=(rate/100)/52;if(term!=0){payment=((amount*interest)/(1-Math.pow((1+interest),-term)));payment1=payment+4.99;$('#result').html('$'+payment1.toFixed(2));}else{$('#result').html('$0.00');}}
if($('#initr').is(':checked')&&$('#gi').is(':checked')&&$('#ci').is(':checked')&&$('#mw').is(':checked')){amount1=parseFloat(ui.value)+((parseFloat(ui.value)*parseFloat($("#loan_period1").val())/100));amount=amount1+601.95+495+350+1000;var term=52*$("#year").val();var rate=22.95;var interest=(rate/100)/52;if(term!=0){payment=((amount*interest)/(1-Math.pow((1+interest),-term)));payment1=payment+4.99;$('#result').html('$'+payment1.toFixed(2));}else{$('#result').html('$0.00');}}}}});$('.ui-slider-handle').append('<span class="sidecar"><input type="text" id="amount" name="amount" readonly value="$1000" /></span>');$("#amount").val($("#slider-range-max").slider("value"));$("#amount1").val($("#slider-range-max").slider("value"));});$(function(){$("#slider-range-new").slider({range:"max",min:1000,max:30000,value:2,step:50,slide:function(event,ui){var amount_with_percent=0;var vdata=ui.value;$("#amount2").val(ui.value);$("#amount-new").val("$"+ui.value);if($("#loan_period2").val()==0)$('#p_result').html('$0.00');else{if($('#p_ci').is(':checked')){amount1=parseFloat(ui.value)+parseFloat(ui.value*parseFloat($("#loan_period2").val())/100);amount=amount1+601.95;var term=52*$("#year1").val();var rate=19.95;var interest=(rate/100)/52;payment=(amount*interest)/(1-Math.pow((1+interest),-term));$('#p_result').html('$'+payment.toFixed(2));}else{amt=parseFloat(ui.value);imt=$("#loan_period2").val();t_amt=parseFloat(amt);a_amt=parseFloat(t_amt)+601.95;p_result
var term=52*$("#year1").val();var rate=19.95;var interest=(rate/100)/52;payment=(a_amt*interest)/(1-Math.pow((1+interest),-term));c_amt=Math.abs(payment);$('#p_result').html('$'+c_amt.toFixed(2));}}}});$('#slider-range-new a.ui-slider-handle').append('<span class="sidecar"><input type="text" readonly id="amount-new" name="amount-new" value="$1000" /></span>');$("#amount-new").val($("#slider-range-new").slider("value"));$("#amount2").val($("#slider-range-new").slider("value"));});function leaveChange(){if(document.getElementById("myList").value==""){$("#loan_period1").val('0');$("#year").val('0');}
if(document.getElementById("myList").value=="1"){$("#loan_period1").val('5.63');$("#year").val('1');yearChange(1);}else if(document.getElementById("myList").value=="2"){$("#loan_period1").val('10.63');$("#year").val('2');yearChange(2);}else if(document.getElementById("myList").value=="3"){$("#loan_period1").val('12.50');$("#year").val('3');yearChange(3);}else if(document.getElementById("myList").value=="4"){$("#loan_period1").val('14.00');$("#year").val('4');yearChange(4);}}
var total=0;var sum=0;var i=0;$(function(){$("input[type=checkbox]").click(function(){if($('#initr').is(':checked')){var amount=parseFloat($("#amount1").val())+601.95+495;var term=52*$("#year").val();var rate=22.95;var interest=(rate/100)/52;if(term!=0){payment=4.99+(amount*interest)/(1-Math.pow((1+interest),-term));$('#result').html('$'+payment.toFixed(2));}else{$('#result').html('$0.00');}}else if($('#ci').is(':checked')){amount1=parseFloat($("#amount1").val())+((parseFloat($("#amount1").val())*parseFloat($("#loan_period1").val())/100));amount=amount1+601.95;var term=52*$("#year").val();var rate=19.95;var interest=(rate/100)/52;if(term!=0){payment=(amount*interest)/(1-Math.pow((1+interest),-term));$('#result').html('$'+payment.toFixed(2));}else{$('#result').html('$0.00');}}else if($('#gi').is(':checked')){amount1=parseFloat($("#amount1").val());amount=amount1+601.95+350;var term=52*$("#year").val();var rate=19.95;var interest=(rate/100)/52;if(term!=0){payment=(amount*interest)/(1-Math.pow((1+interest),-term));$('#result').html('$'+payment.toFixed(2));}else{$('#result').html('$0.00');}}else if($('#mw').is(':checked')){amount=parseFloat($("#amount1").val())+1000+601.95;var term=52*$("#year").val();var rate=19.95;var interest=(rate/100)/52;if(term!=0){payment=(amount*interest)/(1-Math.pow((1+interest),-term));$('#result').html('$'+payment.toFixed(2));}else{$('#result').html('$0.00');}}else{amt=parseFloat($('#amount1').val())+601.95;var term=52*$("#year").val();var rate=19.95;var interest=(rate/100)/52;payment=(amt*interest)/(1-Math.pow((1+interest),-term));$('#result').html('$'+payment.toFixed(2));}
if($('#ci').is(':checked')&&$('#gi').is(':checked')){amount1=parseFloat($("#amount1").val())+((parseFloat($("#amount1").val())*parseFloat($("#loan_period1").val())/100));amount=amount1+601.95+350;var term=52*$("#year").val();var rate=19.95;var interest=(rate/100)/52;if(term!=0){payment=(amount*interest)/(1-Math.pow((1+interest),-term));$('#result').html('$'+payment.toFixed(2));}else{$('#result').html('$0.00');}}
if($('#initr').is(':checked')&&$('#ci').is(':checked')){amount1=parseFloat($("#amount1").val())+((parseFloat($("#amount1").val())*parseFloat($("#loan_period1").val())/100));amount=amount1+601.95+495;var term=52*$("#year").val();var rate=22.95;var interest=(rate/100)/52;if(term!=0){payment=4.99+(amount*interest)/(1-Math.pow((1+interest),-term));$('#result').html('$'+payment.toFixed(2));}else{$('#result').html('$0.00');}}
if($('#initr').is(':checked')&&$('#gi').is(':checked')){amount=parseFloat($("#amount1").val())+601.95+495+350;var term=52*$("#year").val();var rate=22.95;var interest=(rate/100)/52;if(term!=0){payment=4.99+(amount*interest)/(1-Math.pow((1+interest),-term));$('#result').html('$'+payment.toFixed(2));}else{$('#result').html('$0.00');}}
if($('#initr').is(':checked')&&$('#gi').is(':checked')&&$('#ci').is(':checked')){amount1=parseFloat($("#amount1").val())+((parseFloat($("#amount1").val())*parseFloat($("#loan_period1").val())/100));amount=amount1+601.95+495+350;var term=52*$("#year").val();var rate=22.95;var interest=(rate/100)/52;if(term!=0){payment=((amount*interest)/(1-Math.pow((1+interest),-term)));payment1=payment+4.99;$('#result').html('$'+payment1.toFixed(2));}else{$('#result').html('$0.00');}}
if($('#initr').is(':checked')&&$('#gi').is(':checked')&&$('#ci').is(':checked')&&$('#mw').is(':checked')){amount1=parseFloat($("#amount1").val())+((parseFloat($("#amount1").val())*parseFloat($("#loan_period1").val())/100));amount=amount1+601.95+495+350+1000;var term=52*$("#year").val();var rate=22.95;var interest=(rate/100)/52;if(term!=0){payment=((amount*interest)/(1-Math.pow((1+interest),-term)));payment1=payment+4.99;$('#result').html('$'+payment1.toFixed(2));}else{$('#result').html('$0.00');}}});});function personalloan(){if(document.getElementById("p_loan").value==""){$("#loan_period2").val('0');$("#year1").val('0');}
if(document.getElementById("p_loan").value=="1"){$("#loan_period2").val('5.63');$("#year1").val('1');yearChange2(1);}else if(document.getElementById("p_loan").value=="2"){$("#loan_period2").val('10.63');$("#year1").val('2');yearChange2(2);}else if(document.getElementById("p_loan").value=="3"){$("#loan_period2").val('12.50');$("#year1").val('3');yearChange2(3);}else if(document.getElementById("p_loan").value=="4"){$("#loan_period2").val('14.00');$("#year1").val('4');yearChange2(4);}else if(document.getElementById("p_loan").value=="5"){$("#loan_period2").val('14.00');$("#year1").val('5');yearChange2(5);}}
var total=0;var sum=0;var i=0;$(function(){$("input[type=checkbox]").click(function(){if($('#p_ci').is(':checked')){amount1=parseFloat($("#amount2").val())+((parseFloat($("#amount2").val())*parseFloat($("#loan_period2").val())/100));amount=amount1+601.95;var term=52*$("#year1").val();var rate=19.95;var interest=(rate/100)/52;if(term!=0){payment=(amount*interest)/(1-Math.pow((1+interest),-term));$('#p_result').html('$'+payment.toFixed(2));}else{$('#p_result').html('$0.00');}}else{amount1=parseFloat($("#amount2").val());amount=amount1+601.95;var term=52*$("#year1").val();var rate=19.95;var interest=(rate/100)/52;if(term!=0){payment=(amount*interest)/(1-Math.pow((1+interest),-term));$('#p_result').html('$'+payment.toFixed(2));}else{$('#p_result').html('$0.00');}}});});	
	


var currentYear = (new Date).getFullYear();
$(document).ready(function() {
	$("#copyYear").text((new Date).getFullYear());
});

	//set variables references for all the various form elements;
	var $nameCon = $("#name"),
		$emailCon = $("#email"),
		$subjectCon = $("#subject"),
		$messageCon = $("#message"),
		$spamCon = $("#spam"),
		$formFieldsCon = $("input:text, textarea"),
		$statusCon = $("#status"),
		$resetBtn = $("input:reset"),
		$contactForm = $(".contact-form");
	//initialise
	$statusCon.hide(); /*submit handler for contact form*/
	$contactForm.submit(function(e) {
		//prevent default form submit action
		e.preventDefault();
		//clear all error borders from form fields
		$formFieldsCon.removeClass("error-focus");
		//check required fields are not empty and that the email address is valid
		if ($nameCon.val() == "") {
			setStatusMessageCon("Please Enter Your Name");
			$nameCon.setFocus();
		} else if ($emailCon.val() == "") {
			setStatusMessageCon("Please Enter Your Email Address");
			$emailCon.setFocus();
		} else if (!isValidEmailCon($emailCon.val())) {
			setStatusMessageCon("Please Enter a Valid Email Address");
			$emailCon.setFocus();
		} else if ($subjectCon.val() == "") {
			setStatusMessageCon("Please Enter a Subject");
			$subjectCon.setFocus();

		} else if ($messageCon.val() == "") {
			setStatusMessageCon("Please Enter Your Message");
			$messageCon.setFocus();
		} else if (!$spamCon.val() == "") {
			setStatusMessageCon("Spam Attack!!");
		} else {
			//if all fields are valid then send data to the server for processing and didplay "please wait" message
			setStatusMessageCon("Email being sent... please wait");
			//serialize all the form field values as a string
			var formDataCon = $(this).serialize();
			//send serialized data string to the send mail php via POST method
			$.post("mail-send.php", formDataCon, function(sent) {
				if (sent == "error") {
					setStatusMessageCon("Opps, something went wrong - please try again");
				} else if (sent == "success") {
					setStatusMessageCon("Thanks! " + $nameCon.val() + ", your message has been sent!");
					//clear form fields
					$formFieldsCon.val("");
				} //end if else
			}, "html");
		} //end else
	}); //end submit
	//click handler for reset button
	$resetBtn.click(function() {
		$statusCon.slideUp("fast");
		$formFieldsCon.removeClass("error-focus");
	});
	//helper functions

	function setStatusMessageCon(messageCon) {
		$statusCon.html(messageCon).slideDown("fast");
	}
	$.fn.setFocus = function() {
		return this.focus().addClass("error-focus");
	}

	function isValidEmailCon(emailCon) {
		var emailRx = /^[\w\.-]+@[\w\.-]+\.\w+$/;
		return emailRx.test(emailCon);
	}
	


	function isiPhone(){
	    return (
	        //Detect iPhone
	    //var isiPad = navigator.userAgent.match(/iPad/i) != null;
	        (navigator.platform.indexOf("iPhone") != -1) ||
	        //Detect iPod
	        (navigator.platform.indexOf("iPad") != -1)
	    );
	}


	if(isiPhone()){

		$('#auto').css('background-image','url(../i/autoBKG_s.jpg)');
		$('#personal').css('background-image','url(../i/personalBKG_s.jpg)');
		$('#dealer').css('background-image','url(../i/dealerBKG_s.jpg)');
		$('.homeImage').css('background-image','url(../i/homeBKG_s.jpg)');
		$('#about').css('background-image','url(../i/aboutBKG_s.jpg)');

}

	




