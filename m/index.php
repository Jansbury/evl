<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js"> <!--<![endif]-->


    <head>
        <meta charset="utf-8">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />	
        <title>NZCG Finance | Car Loan | Personal Loan | Debt Consolidation</title>
        <meta name="description" content="APPLY ONLINE for a CAR LOAN, PERSONAL LOAN or DEBT CONSOLIDATION through NZCG. Financing your Journey through life. (0800) 002 844">
        <meta name="viewport" content="user-scalable=no,initial-scale = 1.0,maximum-scale = 1.0" />
        <link rel="stylesheet" href="css/m.main.css">
				<link rel="stylesheet" type="text/css" href="css/isitayela.css">
        <script src="js/vendor/modernizr-2.6.2.min.js"></script>
        
    </head>
    <body>
		<header>
			<div class="headerBkg"></div>
		<div class="cntr clearfix">
			<div class="logo home-link">
				<img src="i/logo_sm.svg" alt="logo_thumb" class="svg"/> 
			<span class="logoText">NZ Capital Group <span class="dinLight">Limited</span></span>
			</div>
			<div class="menu-open">
				
			</div>
		</div>
		</header>
		<div class="menuConCon">
			<div class="menuCon">
				<div class="doit">
					<span><a href="tel:0800002844">Phone Us</a></span>
					<hr>
					<span class="applyClick">Apply online</span>
					
				</div>
				
				<nav class="main" id="menu">
					<ul>
						<li class="active"><a class="scroll-to" href="#section0">Welcome</a></li>
						<li><a class="scroll-to" href="#section0a">Loan Calculator</a></li>
						<li><a class="scroll-to" href="#section1">Car Loan</a></li>
						<li><a class="scroll-to" href="#section2">Personal Loan</a></li>
						<li><a class="scroll-to" href="#section3">Contact</a></li>
					</ul>
				</nav>
			</div>
		</div>
		
		
		<div class="cntr">
<!-- WELCOME/CACULATOR -->
			<section id="section0">
				<div class="contentCon">
					<div class="welcomeBkg">
					<img src="i/headerMob.jpg" alt="headerImage"/>
						<div class="welcomeTxt">
							<span>Apply online today</span><br />We can help you with<br />car loans, personal loans<br />and debt consolidation				
						</div>
					</div>
					
					<div class="text">
						<p>We understand that everyone is on a different journey through life and we respect that. We work hard to customise the right lending solution for you; no  matter what direction life is taking.</p>
					</div>
			
			
				<div class="pageTitle" id="section0a">
					<h1>Loan Calculator</h1>
					<hr>
				</div>

			  <div class="calcCon clearfix">
			    <article>
			      <section>
			        <div class="container">
			          <div class="col-md-9 clearfix">
			            <div class="circle">
			              <input type="text" id="amount" name="amount" style="display:inline;" readonly>
			            </div>
			
			            <div id="slider-range-max"></div>
			
			            <div class="col1">
			              <span class="calcNo">$5500</span>
			            </div>
			
			            <div class="col2">
			              <span class="calcNo">$15000</span>
			            </div>
			
			            <div class="col3">
			              <span class="calcNo">$25000</span>
			            </div>
			          </div><!-- <div id="txtAge"></div> -->
			
			          <div class="c-left clearfix">
			            <div class="c-loanY">
			              <div class="col-md-4">
			                <select name="" size="1" id="myList" onchange="leaveChange()">
			                  <option value="">
			                    Select Term
			                  </option>
			
			                  <option value="1">
			                    1 Year
			                  </option>
			
			                  <option value="2">
			                    2 Years
			                  </option>
			
			                  <option value="3">
			                    3 Years
			                  </option>
			
			                  <option value="4">
			                    4 Years
			                  </option>
			                </select> <input type="hidden" id="loan_period1" value="0"> <input type="hidden" id="amount1" value="0"> <input type="hidden" id="percentage1" value="0"> <input type="hidden" id="inittrack" value="0"> <input type="hidden" id="gap" value="0"> <input type="hidden" id="mechwar" value="0"> <input type="hidden" id="ins_id" value="0"> <input type="hidden" id="year" value=""> <input type="hidden" id="perc" value="">
			              </div>
			            </div>
			
			            <div class="c-insur">
			              <div class="c-title">
			                Insurance
			              </div>
			
			              <ul class="c-credI">
			                <li>
			                  <div class="checkbox">
			                    <input type="checkbox" name="select" class="sec" id="ci" value=""> Credit Indemnity <input type="hidden" id="check2" value="0"> <input type="hidden" id="ci_result" value="0">
			                  </div>
			                </li>
			              </ul>
			            </div>
			          </div>
			
			          <div class="c-right clearfix">
			            <div class="c-repay">
			              <h3>Your weekly repayment</h3>
			              <h1 id="result">$0.00</h1>
			            </div>
			          </div>
			        </div><!--end of CONTAINER-->
			      </section>
			    </article>
			  </div>
			          
        <div class="calcDisCon">
          <span class="calcDis">Weekly payments are indicative only, calculated at 19.95% per annum and include loan establishment fees.</span>
        </div>

				<div class="applyBut applyClick">Apply Online</div>
				</div>
			</section>			
<!-- CAR LOAN -->
			<section id="section1">
				<div class="contentCon">
					<div class="pageTitle">
						<h1>Car Loan</h1>
						<hr>	
					</div>
					<div class="headText topSpace">
						ENJOY NO DEPOSIT REGARDLESS OF<br />YOUR CIRCUMSTANCES
						
					</div>
					<div class="headText">
					THE FREEDOM TO BUY WHAT YOU WANT WHERE YOU WANT FROM 12.95% P.A.
					</div>
					
					<div class="processCon">
						<img src="i/progress-01.png" alt="loanProcess">
					</div>
					<div class="applyBut applyClick">Apply Online</div>
				</div>
			</section>
<!-- PERSONAL LOAN -->
			<section id="section2" class="white">
				<div class="contentCon">
					<div class="pageTitle">
						<h1>Personal Loan</h1>
						<hr>	
					</div>
					<div class="headText">
						UP TO 5 YEARS TO PAY<br />1 HOUR APPROVAL. 7 DAYS A WEEK.<br />FROM 15.95% P.A.
						
					</div>
					<div class="text">
						<p>Whatever you’re aiming for, an NZCG Personal Loan could help make it a reality. From $3,000 to $50,000 for almost anything. Consolidate your debts into one low payment, help your children with a first home deposit or follow your dreams and start a new business. How you use your loan is up to you.</p>
					</div>
					
					<div class="processCon">
						<img src="i/progress-02.png" alt="personalProcess">
					</div>
					<div class="applyBut applyClick white">Apply Online</div>
					
				</div>
			</section>
			
<!-- CONTACT/SOCIAL -->
			<section id="section3">
				<div class="contentCon">
				
					<div class="pageTitle">
						<h1>Contact</h1>
						<hr>
					</div>
					
					<ul class="contactInfo">

						<li><a href="https://goo.gl/maps/9ZOwK" target="_blank">Level 3, Walker Wayland Centre<br />53 Fort Street<br />Auckland 1010</a></li>
						<li>PO Box 68591<br />Newton Auckland 1145</li>
						<li><span>P</span><a href="tel:0800002844">0800 002 844</a></li>
						<li><span>F</span>0800 002 845</li>
						<li><a href="mailto:info@nzcapitalgroup.co.nz">info@nzcapitalgroup.co.nz</a></li>
					</ul>
					
					<ul class="downloads">
			    	<li><a href="../pdf/Terms_and_Conditions_(NZCG).pdf" target="_blank">Terms &amp; Conditions</a></li>
			    	<li><a href="../pdf/Privacy_Disclosure_(NZCG).pdf" target="_blank">Privacy</a></li>
			    	<li><a href="../pdf/FDR_Disclosure_(NZCG).pdf" target="_blank">FSP Disclosure</a></li>
					</ul>
					
					<div class="socialCon">
						<div class="pageTitle no"><h1>Connect with us</h1></div>
							<nav class="socialIcons">
					    	<ul>
					    		<li class="none"><a href="https://plus.google.com/app/basic/108321124701962991935" target="_blank"><div class="google">Google+</div></a></li>
						    	<li class="none"><a href="https://twitter.com/nzcapitalgroup" target="_blank"><div class="twitter">Twitter</div></a></li>
						    	<li class="none"><a href="https://www.facebook.com/nzcapitalgroup" target="_blank"><div class="facebook">Facebook</div></a></li>
					    	</ul>
				    	</nav>
					</div>					
				</div>
				<footer>
				<div class="left applyClick">
					Apply Online
				</div>
				<div class="right">
					<a href="tel:0800002844">Phone Us</a>
				</div>
			</footer>
			</section>
			
			
			
		
		</div> <!-- end .cntr -->
		
		<div class="popUp">
			<span class="close"></span>
			<div class="applyCon">
					<span class="onlineText">
						Apply online and one of our lending consultants will contact you within the hour.
					</span>
			
			<!--
		<div class="respon">
						<p>Completing this form should take no more than 5 minutes. Once submitted please send us a copy of your Drivers License or Passport and two months recent Bank Statements. Thank you for choosing NZCG.</p>
						
					</div>
-->
				
				    <form id="dealerForm" action="#" method="post">
					      <div>
					        <input placeholder="First Name" type="text" class="formtext" id="d-Fname" name="FirstName">
					        <input placeholder="Last Name" type="text" class="formtext" id="d-Lname" name="Surname">
					      </div>
					      <div>
					        <input placeholder="Mobile Number" type="tel" class="formtext" id="d-phone" name="MobilePhone">
					      </div>
					      <div>
					        <input placeholder="Email" type="email" class="formtext" id="d-email" name="EmailAddress">
					      </div>
					      <div>
					        <input placeholder="Date of Birth" type="date" class="formtext" id="d-birth" name="DateOfBirth">
					      </div>
				     		<div style="clear:both;">
									<p class="d-question">How much do you want to borrow?</p>
									<input placeholder="$0.00" type="number" class="formtext" id="d-borrow" name="Amount">
								</div>
								
							 <div class="budge">
						      <fieldset>
										<span class="questions">Loan Type:</span>
									  <div class="field switch yandn">
											<input type="radio" name="q0" id="q0_car" class="radioYes" value="Car Loan" />
											<label for="q0_car" class="cb-enable after_radio"><span>Car Loan</span></label>
											<input type="radio" name="q0" id="q0_per" value="Personal Loan" />
											<label for="q0_per" class="cb-disable after_radio"><span>Personal</span></label>
										</div>
								  </fieldset>	
					     </div>
								
							 <div>
						     <fieldset>
										<span class="questions">Are you over 18?</span>
									  <div class="field switch yandn">
											<input type="radio" name="q1" id="q1_yes" class="radioYes" value="Yes" />
											<label for="q1_yes" class="cb-enable after_radio"><span>Yes</span></label>
											<input type="radio" name="q1" id="q1_no" value="No" />
											<label for="q1_no" class="cb-disable after_radio"><span>No</span></label>
										</div>
								  </fieldset>	
					     </div>
					     
					     <div>
						     <fieldset>
										<span class="questions">Are you a NZ citizen or permanent resident?</span>
									  <div class="field switch yandn">
											<input type="radio" name="q2" id="q2_yes" class="radioYes" value="Yes" />
											<label for="q2_yes" class="cb-enable after_radio"><span>Yes</span></label>
											<input type="radio" name="q2" id="q2_no" value="No" />
											<label for="q2_no" class="cb-disable after_radio"><span>No</span></label>
										</div>
								  </fieldset>	
					     </div>
					     <div>
						     <fieldset>
										<span class="questions">Do you receive a regular income?</span>
									  <div class="field switch yandn">	
											<input type="radio" name="q3" id="q3_yes" class="radioYes" value="Yes" />
											<label for="q3_yes" class="cb-enable after_radio"><span>Yes</span></label>
											<input type="radio" name="q3" id="q3_no" value="No" />
											<label for="q3_no" class="cb-disable after_radio"><span>No</span></label>
										</div>
								  </fieldset>	
					     </div>
								<p id="status"></p>
				      <div>
				        <input class="radiobutton" type="submit"  value="Submit Application">
				      </div>
				    </form>
			
				
				
				
			</div>
		</div>
		
		
		   	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
			 	<script src="js/vendor/jquery-ui.js"></script>
				<script src="js/m.main.js"></script>
				<script src="js/math.js"></script>



  
<!--
			<script>
			  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
			
			  ga('create', 'UA-41471063-2', 'nzcapitalgroup.co.nz');
			  ga('send', 'pageview');
			</script>
-->
    </body>
</html>